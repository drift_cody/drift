<?php include('header.php'); ?>

    <div class="color-block"></div>
    <section class="selecting-players">
        <div class="container " >
            <h1>Select Players</h1>
            <div class="players">
                <h5>SEE ALL PLAYERS</h5>
                <div class="down-arrow"></div>
                <div class="select-players">
                    <label for="player1">
                        <input type="checkbox" id="player1">
                        <p class="name">Wade</p>
                        <p class="score">15</p>
                        <div class="add"></div>
                    </label>
                    <label for="player2">
                        <input type="checkbox" id="player2">
                        <p class="name">Gavin</p>
                        <p class="score">14</p>
                        <div class="add"></div>
                    </label>
                </div>
            </div>
        </div>
    </section>

<?php include('footer.php'); ?>

