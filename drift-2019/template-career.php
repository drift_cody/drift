<?php if (have_rows('s3_flexible_content')) : ?>
    <?php while (have_rows('s3_flexible_content')) : the_row(); ?>

        <?php if (get_row_layout() == 'image_row') : ?>
            <!-- //images rep
            //image -->
            <div class="image-row">
                <?php if (have_rows('images')) : ?>
                    <?php while (have_rows('images')) : the_row(); ?>
                        <div class="image">
                            <img src="<?php echo esc_url(get_sub_field('image')['url']); ?>" alt="">
                        </div>
                <?php endwhile;
                            endif; ?>
            </div>
        <?php endif; ?>

        <?php if (get_row_layout() == 'video_row') : ?>
            <!-- //image//video_type//youtube_embeded//video_upload//thumbnail -->
            <div class="video-row-career ">
                <div class="image">
                    <img src="<?php echo esc_url(get_sub_field('image')['url']); ?>" />
                </div>
                <div class="video">
                    <?php if (get_sub_field('video_type') == 'Upload') : ?>
                        <div class="video-container">
                            <video autoplay muted loop>
                                <source src="<?php the_sub_field('video_upload'); ?>">
                            </video>
                        </div>
                    <?php endif; ?>
                    <?php if (get_sub_field('video_type') == 'YouTube') : ?>
                        <div class="video-container youtube">
                            <img src="<?php echo esc_url(get_sub_field('thumbnail')['url']); ?>" alt="">
                            <div class="play"></div>
                        </div>
                        <div class="video-popup">
                            <button class="close-it"></button>
                            <div class="vid-width">
                                <div class="vid-con">
                                    <?php the_sub_field('youtube_embed'); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>