<?php if (have_rows('bottom_content')) : ?>

	<?php while (have_rows('bottom_content')) : the_row(); ?>

		<!-- Begin image row -->
		<?php if (get_row_layout() == 'image_row') : ?>
			<div class="image-row">
				<?php if (have_rows('images')) : ?>
					<?php while (have_rows('images')) : the_row(); ?>
						<div class="image">
							<img class="work-img" src="<?php the_sub_field('image'); ?>" />
							<?php if (get_sub_field('text')) : ?>
								<div class="eyeball"></div>
								<div class="hidden-text">
									<div class="text">
										<?php the_sub_field('text'); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- End image row -->

		<!-- Begin text row -->
		<?php if (get_row_layout() == 'text_row') : ?>
			<div class="text-row">
				<div class="text contained">
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End text row -->

		<!-- Begin static web page screenshots -->
		<?php if (get_row_layout() == 'static_web_page_screenshots') : ?>
			<div class="static-web-screenshots-row">
				<div class="left-column">
					<div class="text">
						<?php the_sub_field('text'); ?>
					</div>
					<?php if (get_sub_field('left_image')) : ?>
						<img src="<?php the_sub_field('left_image'); ?>" />
					<?php endif; ?>
				</div>
				<div class="right-column">
					<?php if (get_sub_field('right_image')) : ?>
						<img src="<?php the_sub_field('right_image'); ?>" />
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End static web page screenshots -->

		<!-- Begin scrolling text -->
		<?php if (get_row_layout() == 'scrolling_text') : ?>
			<div class="scrolling-text" style="background:<?php the_sub_field('background_color'); ?>">
				<div class="sticky-anchor"></div>
				<div class="text sticky">
					<?php the_sub_field('text'); ?>
				</div>
				<div class="image">
					<img src="<?php the_sub_field('image'); ?>" />
				</div>
			</div>
			<div class="scrolling-text-end"></div>
		<?php endif; ?>
		<!-- End scrolling text -->

		<!-- Begin video row -->
		<?php if (get_row_layout() == 'video_row') : ?>
			<div class="video-row">
				<div class="image">
					<img src="<?php the_sub_field('image'); ?>" />
				</div>
				<div class="video">
					<?php if (get_sub_field('video_type') == 'Upload') : ?>
						<div class="video-container">
							<video autoplay muted loop>
								<source src="<?php the_sub_field('video_upload'); ?>">
							</video>
						</div>
					<?php endif; ?>
					<?php if (get_sub_field('video_type') == 'YouTube') : ?>
						<div class="video-container youtube">
							<img src="<?php the_sub_field('thumbnail'); ?>" />
							<div class="play"></div>
							<div class="video-popup">
								<div class="vid-width">
									<div class="vid-con">
										<?php the_sub_field('youtube_embed'); ?>
										<div class="close-it"></div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End video row -->

		<!-- Begin stats row -->
		<?php if (get_row_layout() == 'stats_row') : ?>
			<div class="stats-row">
				<div>
					<p class="all-in">Its all in the stats</p>
					<div class="stats">
						<?php if (have_rows('stats')) : ?>
							<?php while (have_rows('stats')) : the_row(); ?>
								<div class="stat">
									<h1 class="big-number"><?php the_sub_field('big_text'); ?></h1>
									<h3><?php the_sub_field('small_text'); ?></h3>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<!-- End stats row -->

		<!-- Begin Slideshow -->
		<?php if (get_row_layout() == 'slideshow') : ?>
			<div class="slickSlideShow">
				<?php if (have_rows('images')) : ?>
					<?php while (have_rows('images')) : the_row(); ?>
						<div class="img-cover">
							<img src="<?php the_sub_field('image'); ?>" alt="" class="cover">
							<div class="buttons">
								<div class="slide-left"></div>
								<div class="slide-right"></div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- End Slideshow -->


	<?php endwhile; ?>
<?php endif; ?>