<footer>
	<div class="bottom-footer">
		<img class="lightning" src="<?php echo get_bloginfo('template_directory');?>/images/lightning.svg" />
		<p>POWERED BY DRIFT</p>
	</div>
</footer>

		<?php wp_footer(); ?>

		<script>
			if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
			    skrollr.init({
			        forceHeight: false
			    });
			}
			if($(window).width() < 980){
				skrollr.init().destroy();
			}
		</script>

		<script>
			imagesLoaded( document.querySelectorAll('img'), () => {
				document.body.classList.remove('loading');
			});

			Array.from(document.querySelectorAll('.case-study-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});


		</script>

	    <!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/409445/animateAnything.js"></script>
	
	</body>
</html>