<?php get_header(); ?>

<main class="work-page">

	<section class="s1-work">
		<h1 class="lapture b58"><?php the_field('page_title'); ?></h1>
		<p class="sub-text"><?php the_field('location'); ?></p>
		<div class="-row">
			<?php if(get_field('main_video_checkbox')):?>
				<div class="header-hero header-hero__line">
					<video width="100%" autoplay loop class="hero-video" muted="muted">
						<source src="<?php echo esc_url(get_field('main_video'));?>" type="video/mp4">
					</video>
				</div>
			<?php else: ?>
			<div class="header-hero header-hero__line">
				<img src="<?php the_field('main_image'); ?>" alt="" class="header-hero__image">
			</div>
			<?php endif; ?>
		</div>
		<!-- <div class="main-image" style="background:<?php the_field('background_color'); ?>">
			<img src="<?php the_field('main_image'); ?>" />
		</div> -->
	</section>

	<section class="s2-work">
		<div class="text">
			<h2 class="contained lapture b58"><?php the_field('headline_text'); ?></h2>
			<div class="more-text">
				<div class="paragraph">
					<?php the_field('paragraph_text'); ?>
					<?php $button = get_field('website_link'); ?>

					<?php if ($button) : $link_url = $button['url'];
						$link_title = $button['title'];
						$link_target = $button['target'] ? $button['target'] : '_self'; ?>
						<a class="main-button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					<?php endif; ?>
				</div>
				<div class="list">
					<p class="sub-text">What we did</p>
					<?php the_field('what_we_did'); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="s3-work">
		<?php get_template_part('template', 'single-work'); ?>
	</section>

	<?php
	$next_post = get_next_post();
	if (!empty($next_post)) :
		?>
		<section class="s4-work">
			<div class="line"></div>
			<p class="gotham">ON TO THE NEXT ONE</p>
			<p class="next"><a class="dot-link next" href="<?php echo esc_url(get_permalink($next_post->ID)); ?>"><?php echo esc_attr($next_post->post_title); ?></a></p>
		</section>
	<?php else : ?>
		<section class="s4-work">
			<div class="line"></div>
			<p class="gotham">BACK TO OUR WORK</p>
			<p class="next"><a class="dot-link next" href="/work-overview">ALL Work</a></p>
		</section>
	<?php endif; ?>

</main>

<script>
	$(document).ready(function() {
		$('.slickSlideShow').slick({
			slidesToShow: 1,
			prevArrow: '.slide-left',
			nextArrow: '.slide-right',
			fade: true,
			adaptiveHeight: true

		});
		if(document.getElementsByClassName('hero-video')){
			const videos = document.getElementsByClassName('hero-video');
			for(video of videos){
				video.play();
			}
		}
	});

	function sticky_relocate() {
		var window_top = $(window).scrollTop();
		console.log('window top: '+window_top);

		var footer_top = $(".scrolling-text-end").offset().top;
		console.log('footer top: '+footer_top);

		var div_top = $('.sticky-anchor').offset().top;
		console.log('div top: '+div_top);

		var div_height = $(".sticky").height();
		console.log('div height: '+div_height);


		var padding = 180; // tweak here or get from margins etc

		var scroll = $(window).scrollTop();

		if ($('body').hasClass('drop') && $(window).scrollTop() > 350) {
			//$('html').stop().animate({ scrollTop: $(window).scrollTop() - 350}, 400, 'linear');
			$('body').removeClass('drop');
			$('.dropdown-newsletter').removeClass('drop');
		}

		if (window_top + div_height > footer_top - padding) {
			$('.sticky').css({
				top: (window_top + div_height - footer_top + padding) * -1
			});
		} else if (window_top > div_top) {
			$('.sticky').addClass('stick');
			$('.sticky').css({
				top: 0
			});
		} else {
			$('.sticky').removeClass('stick');
		}
	}

	$(function() {
		$(window).scroll(sticky_relocate);
		sticky_relocate();
	});

	$(".eyeball").click(function() {
		$(this).parent().toggleClass("active");
	});

	$(".youtube .play").click(function() {
		$(this).parent().find(".video-popup").addClass("active");
		$("body").addClass("no-scroll");
	});

	$(".close-it").click(function() {
		$(".video-popup.active").removeClass("active");
		$("body").removeClass("no-scroll");
	});


	// If image row has text add class
	$('.image-row-block').each(function() {
		if ($(this).find('div.text').length !== 0) {
			$(this).addClass('img-text-block');
		}
	});
	// if there is a image-row-1x3 controls the play/pause
	$('.image-video-3x__content-container').click(function() {
		$(this).children('video').attr("controls", "");
		$(this).children('.image-video-3x__play-button').fadeToggle();
	})
</script>

<?php get_footer(); ?>