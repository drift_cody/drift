<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $alt=get_post_thumbnail_id(); $alt = get_post_meta($alt, '_wp_attachment_image_alt', true);?>
<main class="post">

    <section class="s1-post m-130">
        <h1><?php echo the_title();?></h1>
        <p class="gotham"><?php echo the_date();?></p>
        <div class="img-cover">
            <!-- NEEDS ALT TAG -->
            <img src="<?php echo esc_url(get_the_post_thumbnail_url()); ?>" alt="<?php echo esc_attr($alt);?>" class="cover">
        </div>
		</section>
		

    <section class="s2-post">
        <div class="by-line">
            <div class="author">
                <div class="avatar">
                    <? echo get_avatar( get_the_author_meta('user_email')); ?>
                  
                </div>
                <div class="author-name">
                    <p class="lapture">Author</p>
                    <p class="gothom author-name"><?php echo get_the_author(); ?></p>
                </div>
            </div>
            <div class="social">
                <p class="lapture">Spread the Love</p>
                <ul class="ns">
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"  target="_blank" class="icon-fb_icon"></a></li>
                    <li><a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" class="icon-twitter"></a></li>
                    <!-- <li><a href="" class="ig"></a></li> -->
                </ul>
            </div>
            <div class="tags">
                <p class="lapture">Tags</p>
                <ul class="ns">
                    <!-- <?php the_tags(); ?> -->
                    <?php $post_tags = get_the_tags(); 
                        if($post_tags){
                            foreach( $post_tags as $tag){?>
                               <li><a href="<?php echo get_site_url(null, '/tag/'.$tag->name) ?>"> #<?php echo $tag->name; ?> </a></li>
                            <?php }
                        }
                    ?>
                </ul>
            </div>
        </div>
        <div class="content">
						<?php the_content(); ?>
						<div class="blue-line"></div>
        </div>
    </section>
</main>
<section class="blog-posts-navy">
        <div class="left">
					<h2 class="b58">Other posts you may want to take a look at</h2>
        </div>
        <div class="right">
            <ul class="ns">
            <?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                <li>
                    <a href="<?php the_permalink() ?>">
                        <p class="gotham"><?php the_time('M d'); ?></p>
                        <h2 class="b40"><?php the_title(); ?></h2>
                    </a>
                </li>
                <?php 
                endwhile;
                wp_reset_postdata();
            ?>
                
            </ul>
            <a href="<?php echo get_site_url(null, '/blog') ?>" class="main-button -navy"><p>BACK TO BLOG</p></a>
        </div>
    </section>
<?php endwhile; endif;?>

<?php get_footer(); ?>
