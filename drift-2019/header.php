<!DOCTYPE html>
<html lang="en">
<!-- site by

▓█████▄  ██▀███   ██▓  █████▒▄▄▄█████▓
▒██▀ ██▌▓██ ▒ ██▒▓██▒▓██   ▒ ▓  ██▒ ▓▒
░██   █▌▓██ ░▄█ ▒▒██▒▒████ ░ ▒ ▓██░ ▒░
░▓█▄   ▌▒██▀▀█▄  ░██░░▓█▒  ░ ░ ▓██▓ ░ 
░▒████▓ ░██▓ ▒██▒░██░░▒█░      ▒██▒ ░ 
 ▒▒▓  ▒ ░ ▒▓ ░▒▓░░▓   ▒ ░      ▒ ░░   
 ░ ▒  ▒   ░▒ ░ ▒░ ▒ ░ ░          ░    
 ░ ░  ░   ░░   ░  ▒ ░ ░ ░      ░      
   ░       ░      ░                   
 ░                                    

-->
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>
	    	<?php
				/*
				 * Print the <title> tag based on what is being viewed.
				 */
				global $page, $paged;

				wp_title( '|', true, 'right' );

				// Add the blog name.
				bloginfo( 'name' );
			?>
		</title>

	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		

		<?php wp_head(); ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8366979-3"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-8366979-3');
		</script>
		<script> (function(){ window.ldfdr = window.ldfdr || {}; (function(d, s, ss, fs){ fs = d.getElementsByTagName(s)[0]; function ce(src){ var cs = d.createElement(s); cs.src = src; setTimeout(function(){fs.parentNode.insertBefore(cs,fs)}, 1); } ce(ss); })(document, 'script', 'https://sc.lfeeder.com/lftracker_v1_lAxoEaKgxZAaOYGd.js'); })(); </script>
	</head>
	<body>
		<section class="dropdown-newsletter">
	        <div class="newsletter__content u-pad">
	            <div class="newsletter__content__left">
	                <p class="p-25">SIGN UP FOR OUR NEWSLETTER</p>
	                <h2 class="b58">Marketing and branding tips that convert</h2>
	            </div>
	            <div class="newsletter__content__right">
	                <!-- Begin Mailchimp Signup Form -->
	                <div id="mc_embed_signup">
	                    <form action="https://driftingcreatives.us12.list-manage.com/subscribe/post?u=cbe005d9830b5e01dbd327281&amp;id=de707a89a1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	                        <div id="mc_embed_signup_scroll">

	                            <div class="mc-field-group">
	                                <label for="mce-EMAIL">What's your email address?<span>*</span></label>
	                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="hello@driftingcreatives.com">
	                            </div>
	                            <div id="mce-responses" class="clear">
	                                <div class="response" id="mce-error-response" style="display:none"></div>
	                                <div class="response" id="mce-success-response" style="display:none"></div>
	                            </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cbe005d9830b5e01dbd327281_de707a89a1" tabindex="-1" value=""></div>
	                            <div class="clear icon-arrow-nc"><input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button "></div>
	                        </div>
	                    </form>
	                </div>

	                <!--End mc_embed_signup-->
	                <!-- <div class="input">
	                    <label for="email">Whats your email address?<span>*</span></label>
	                    <input type="email" placeholder="hello@driftingcreatives.com">
	                </div> -->
	            </div>
	        </div>
		</section>
		<header>
			<div class="header-section">
				<div class="dot-link localizing-b2b">NEWSLETTER SIGN UP</div>
			</div>

			<div class="header-section center" >
				<div class="img-cover">
					<img src="<?php echo get_bloginfo('template_directory');?>/images/logo.svg" />
					<img class="white" src="<?php echo get_bloginfo('template_directory');?>/images/logo-white.svg" />
					<a href="/"></a>
				</div>
				
			</div>

			<div class="header-section right">
				<!-- <button class="menu-activator" aria-haspopup="true"  aria-expanded="false" aria-controls="menu" aria-label="Menu">
					<div class="hamburger"></div>
				</button> -->
				<div class="nav-background"></div>
				<div class="menu-activator" id="b3">
					<svg id="i1" class="icon" viewBox="0 0 100 100">
					<path id="top-line-1"     d="M30,37 L70,37 Z"></path>
					<path id="middle-line-1" 	d="M30,50 L70,50 Z"></path>
					<path id="bottom-line-1" 	d="M30,63 L70,63 Z"></path>
					</svg>
				</div>
			</div>
			<div class="nav-container">
				<div class="content">
					<div class="menu-header">
						<div class="header-section left"></div>
						<div class="header-section center">
							<div class="img-cover">
								<img class="" src="<?php echo get_bloginfo('template_directory');?>/images/logo-white.svg" />
								<a href="/"></a>
							</div>
						</div>
						<div class="header-section right"></div>
					</div>
					<nav class="nav-navigation"><?php wp_nav_menu(array('theme_location'=>'primary','container'=>'div','container_id'=>'nav-menu')); ?></nav>
					<div class="bottom">
						<div class="number-block">
							<p class="gotham">CALL US! WE'RE FRIENDLY</p>
							<p class="phone lapture"><?php echo the_field('phone_number', 'option') ?></p>
						</div>
						<div class="email-block">
							<p class="gotham">WE LOVE GETTING EMAILS</p>
							<p class="email lapture"><?php echo the_field('company_wide_email', 'option'); ?></p>
						</div>
						<div class="social-block">
							<p class="gotham">GET SOCIAL</p>
							<ul>
								<li class=""><a href="https://www.instagram.com/driftcreate" target="_blank" class="icon-insta_icon"></a></li>
								<li class=""><a href="https://www.facebook.com/pg/DriftingCreatives/" target="_blank" class="icon-fb_icon"></a></li>
								<li class=""><a href="https://twitter.com/driftcreate" target="_blank" class="icon-twitter"></a></li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		
		<!-- <div class="hidden-nav" aria-hidden="true">
			<div class="menu-content">
				<nav>
					<?php wp_nav_menu(array('theme_location'=>'primary','container'=>'div','container_id'=>'nav-menu')); ?>
				</nav>
				
				<div class="bottom">
					<div class="number-block">
						<p class="gotham">CALL US! WE'RE FRIENDLY</p>
						<p class="phone lapture"><?php echo the_field('phone_number', 'option') ?></p>
					</div>
					<div class="email-block">
						<p class="gotham">WE LOVE GETTING EMAILS</p>
						<p class="email lapture"><?php echo the_field('company_wide_email', 'option'); ?></p>
					</div>
					<div class="social-block">
						<p class="gotham">GET SOCIAL</p>
						<ul>
							<li class=""><a href="facebook.com" target="_blank" class="icon-insta_icon"></a></li>
							<li class=""><a href="facebook.com" target="_blank" class="icon-fb_icon"></a></li>
							<li class=""><a href="facebook.com" target="_blank" class="icon-twitter"></a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</div> -->