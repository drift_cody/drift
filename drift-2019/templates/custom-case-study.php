<?php
/*
Template Name: Case Study
*/
?>

<?php get_header(); ?>
<main class="case-study-overview">
    <div class="case-studies">

        <?php
        if (have_rows('case_studies')) :
            while (have_rows('case_studies')) : the_row(); ?>
                <div class="case-study">
                    <div class="img-cover">
                        <a href="<?php the_sub_field('page'); ?>"></a>
                        <img src="<?php the_sub_field('featured_image'); ?>" alt="" class="cover">
                    </div>

                    <h2 class="b58"><a href="<?php the_sub_field('page'); ?>"><?php the_sub_field('title'); ?></a></h2>
                    <?php if (get_sub_field('sub_text')) : $tags = get_sub_field('sub_text'); ?>
                        <ul class="ns">
                            <?php foreach ($tags as $tag) : ?>
                                <li style="text-transform: uppercase"><?php echo $tag; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
        <?php endwhile;
        endif; ?>

    </div>
    <div class="more">
        <div class="line"></div>
        <p>LIKE WHAT YOU SEE?</p>
        <div class="link">
            <a href="/work-overview" class="dot-link">More of Our Work</a>
        </div>
    </div>
</main>
<?php get_footer(); ?>

<!-- title
page
sub_text -->