<?php
/*
Template Name: Logos
*/
?>


<?php get_header(); ?>
<main class="logos">
    <section class="s1-logos m-150 g-side-by-side-header">
        <div class="left">
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('header_images')['left_image']['url']); ?>" alt="<?php echo esc_attr(get_field('header_images')['left_image']['alt']); ?>" class="cover">
            </div>
        </div>
        <div class="right">
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('header_images')['right_image']['url']); ?>" alt="<?php echo esc_attr(get_field('header_images')['right_image']['alt']); ?>" class="cover">
            </div>
        </div>
    </section>
    <section class="s2-logos m-130">
        <div class="text">
            <?php the_field('text'); ?>
        </div>
    </section>

    <section class="s3-logos m-400">
        <ul class="ns logos">
           <?php if(have_rows('logos')): ?>
                <?php while(have_rows('logos')): the_row();
                    $image = get_sub_field('logo_image');
                    $name = get_sub_field('company_name');
                ?>
            
            <li>
                <div class="">
                    <img class='' src="<?php echo esc_url($image['url']) ?>" alt="<?php echo esc_attr( $image['alt']) ?>">
                </div>
            </li>
            <? endwhile; endif;?>
        </ul>
    </section>

    <section class="s4-logos m-400">
        <div class="text">
            <?php echo get_field('second_text_section')['text']; ?>
        </div>
        <a href="<?php echo esc_url(get_field('second_text_section')['button_link']); ?>" class="main-button"><?php echo get_field('second_text_section')['button']; ?></a>
    </section>

    <section class="s5-logos m-150">
        <?php $image_group = get_field('images');?>
        <div class="image-row">
            <div class="image">
                <img src="<?php echo esc_url($image_group['top_left_image']['url']) ?>" alt="<?php echo esc_attr($image_group['top_left_image']['alt']) ?>" class="work-img">
            </div>
            <div class="image">
                <img src="<?php echo esc_url($image_group['top_right_image']['url']) ?>" alt="<?php echo esc_attr($image_group['top_right_image']['alt']) ?>" class="work-img">
            </div>
        </div>
        <div class="image-row">
            <div class="image">
             <img src="<?php echo esc_url($image_group['center_image']['url']) ?>" alt="<?php echo esc_attr($image_group['center_image']['alt']) ?>" class="work-img">
            </div>
        </div>
        <div class="image-row">
            <div class="image">
                <img src="<?php echo esc_url($image_group['bottom_left_image']['url']) ?>" alt="<?php echo esc_attr($image_group['bottom_left_image']['alt']) ?>" class="work-img">
            </div>
            <div class="image">
             <img src="<?php echo esc_url($image_group['bottom_right_image']['url']) ?>" alt="<?php echo esc_attr($image_group['bottom_right_image']['alt']) ?>" class="work-img">
            </div>
        </div>
    </section>
    <section class="s6-logos m-100">
        <div class="line-group">
            <div class="line"></div>
        </div>
        <p class="gotham">READY TO TELL US ABOUT YOUR PROJECT?</p>
        <a href="" class="gotham dot-link">Contact Us</a>
    </section>
</main>


<?php get_footer(); ?>