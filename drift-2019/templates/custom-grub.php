<?php
/*
Template Name: Case Study Grub
*/
?>

<?php get_header(); ?>
<main class="case-study grub">
    <section class="s1-grub case-study-s1 m-400">
        <p class="superscript">BURGER BAR & KITCHEN</p>
        <h1 class="b120">Grub</h1>
    </section>
    <section class="case-study-s2 s2-grub">
        <div class="img-cover">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/hero-image.jpg" alt="" class="cover">
        </div>
        <div class="bottom-content">
            <div class="story">
                <h2 class="b40">Grub is all about good community, good times, and good food. </h2>
                <div class="story-content">
                    <h3 class="b20 gotham">THE STORY</h3>
                    <p>After some explosive growth (25+ locations) Grub came to us in the midst of an identity crisis. Their current brand wasn’t speaking to the right audience. We helped redevelop their digital brand to reflect a more targeted demographic. The new site is ADA accessible, and has a number of amazing upgrades for screen readers. </p>
                </div>
            </div>
            <h3 class="b20 gotham">WHAT WE DID</h3>
            <ul class="ns">
                <li>Digital rebrand</li>
                <li>Custom website design</li>
                <li>Copywriting</li>
                <li>Web animations</li>
                <li>Custom wordpress</li>
                <li>Multi-tiered pricing system</li>
                <li>25+ unique location menus</li>
            </ul>
            <a href="https://grubkitchenandbar.com/"  target="_blank" class="main-button">Visit the website</a>
        </div>
    </section>
    <div class="case-study-content">
        <div class="menu-anchor-top"></div>
        <div class="side-nav-container sticky" aria-hidden="true">
            <div class="side-nav bottom" aria-hidden="true">
                <ul>
                    <li class=""><a href="#design-system"  ><p class="gotham">DESIGN SYSTEM</p></a></li>
                    <li class=""><a href="#website" ><p class="gotham">WEBSITE</p></a></li>
                    <li class=""><a href="#responsive" ><p class="gotham">RESPONSIVE</p></a></li>
                </ul>
            </div>
        </div>

        <section class="s3-grub" id="design-system">
            <div class="top">
                <h2 class="b173" >Design<br><span>System</span></h2>
                <div class="right">
                    <div class="img-cover">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/colors.png" alt="" class="cover">
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="left">
                    <div class="img-cover">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/typography.jpg" alt="" class="cover">
                    </div>
                </div>
                <div class="right">
                    <div class="img-cover">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/buttons.jpg" alt="" class="cover">
                    </div>
                </div>

            </div>
        </section>
        <section class="s4-grub" id="website">
            <div class="left">
                <h2 class="b173" >Website</h2>
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site8.jpg" alt="" class="cover">
                </div>
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site3.jpg" alt="" class="cover">
                </div>

            </div>
            <div class="right">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site-1.jpg" alt="" class="cover">
                </div>
            </div>
        </section>
        <section class="s5-grub">
            <h2 class="b58">Website design is evolving, and web accessibility is critical to any new site. We solved a number of unique challenges bridging the gap between a great user experience for someone with sight versus someone using a screen reader.</h2>
        </section>
        <section class="s6-grub">
            <div class="hoverbox">
                <div class="on-hover">
                    <p>Bring</p>
                    <p>The</p>
                    <p>Heat</p>
                </div>
            </div>
            <div class="img-cover"><img src="<?php bloginfo('template_url');?>/images/case-studies/grub/homepage.jpg" alt="" class="cover"></div>
        </section>
        <section class="s7-grub" id="responsive">
            <h2 class="b173" >Responsive</h2>
            <div class="img-cover">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site4.jpg" alt="" class="cover">
            </div>
            <div class="bottom">
                <div class="text">
                    <p class="number">70%</p>
                    <h3 class="b26">Since 70% of Grub’s users are on mobile, we focused on users being able to easily navigate and order from a phone or iPad.</h3>
                </div>
                <div class="image">
                    <div class="img-cover"><img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site6.jpg" alt="" class="cover"></div>
                </div>
                <div class="image">
                    <div class="img-cover"><img src="<?php bloginfo('template_url');?>/images/case-studies/grub/site5.jpg" alt="" class="cover"></div>
                </div>
            </div>
        </section>
        <!-- <section class="s8-grub" id="results">
            <h2 class="b173">Results</h2>
            <div class="list">
                <div class="item">
                    <p class="number">15%</p>
                    <p class="text">We helped Grub grow their online orders by 15% over 6 months. Yea, that’s money.</p>
                </div>
                <div class="item">
                    <p class="number">26</p>
                    <p class="text">We helped Grub grow their online orders by 15% over 6 months. Yea, that’s money.</p>
                </div>
                <div class="item">
                    <p class="number">254</p>
                    <p class="text">We helped Grub grow their online orders by 15% over 6 months. Yea, that’s money.</p>
                </div>
            </div>
        </section> -->
        <section class="s9-grub last">
            <div class="line"></div>
            <p class="gotham">More work this way</p>
            <div class="next">
                <a href="/eagle-metal" class="dot-link next">Eagle Metal</a>
            </div>
        </section>

        <div class="menu-anchor-end"></div>
    </div>
</main>
<script>
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".menu-anchor-end").offset().top;
        var div_top = $('.menu-anchor-top').offset().top;
        var div_height = $(".sticky").height();
        var padding = 0;  // tweak here or get from margins etc
        
        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({top: (window_top + div_height - footer_top + padding) * -1})
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.stick').css({top:0})
        } else {
            $('.sticky').removeClass('stick');
        }
    }
    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
        
    });
</script>
<?php get_footer(); ?>