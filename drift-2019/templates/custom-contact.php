<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>
<main class="contact">
    <section class="s1-contact m-150 g-side-by-side-header">
        <div class="left">
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('header_images')['left_image']['url']); ?>" alt="<?php echo esc_attr(get_field('header_images')['left_image']['alt']); ?>" class="cover">
            </div>
        </div>
        <div class="right">
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('header_images')['right_image']['url']); ?>" alt="<?php echo esc_attr(get_field('header_images')['right_image']['alt']); ?>" class="cover">
            </div>
        </div>
    </section>
    <section class="s2-contact">
        <div class="-content">
            <?php the_field('text'); ?>
        </div>
        <div class="line-group">
            <div class="line"></div>
            <div class="circle icon-paper-plane"></div>
        </div>
    </section>
    <section class="s3-contact m-230">
        
        <?php echo do_shortcode( '[contact-form-7 id="11852" title="Contact form 1"]' ); ?>
        <form action=""></form>
    </section>
    <section class="s4-contact m-150">
        <p class="gotham">REACH OUT TO US AT ANY TIME!</p>
        <ul class="ns">
            <li class="item">
                <div class="circle icon-fb_icon"></div>
                <p class="title lapture">Drop a Message</p>
                <p class="email"><?php echo the_field('company_wide_email', 'option');?></p>
                <p class="number"><?php echo the_field('phone_number', 'option');?></p>
            </li>
            <li class="item">
                <div class="circle icon-DriftD"></div>
                <p class="title lapture">Visit our Headquarters</p>
                <p class="address"><?php echo the_field('address', 'option');?></p>
                <p class="address"><?php echo the_field('address_ln2', 'option');?></p>
                
            </li>
            <li class="item">
                <div class="circle icon-locations_marker"></div>
                <p class="title lapture">Dallas Office</p>
                <p class="dallas_address"><?php echo the_field('dallas_address', 'option');?></p>
                <p class="dallas_address"><?php echo the_field('dallas_address_ln2', 'option');?></p>
            </li>
            <li class="item">
                <div class="circle icon-suitcase"></div>
                <p class="title lapture">Career Opportunities</p>
                <p class="learn">We’re always looking for creative people to join our team. <a href="/careers">Learn More!</a></p>
            </li>
        </ul>
    </section>
    <section class="s5-contact m-150">
        <img src="<?php echo esc_url(get_field('bottom_image')['url']); ?>" alt="<?php echo esc_attr(get_field('bottom_image')['alt']); ?>" class="">
    </section>


</main>
<script>
     $('input[type=checkbox]').click(function(){
        $('input[type=checkbox]').each(function(){
            if(this.checked){
                $(this).parent().addClass('active');
            }else{
                $(this).parent().removeClass('active');
            }
        });
     });
   
</script>
<?php get_footer(); ?>