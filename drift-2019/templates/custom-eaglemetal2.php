<?php
/*
Template Name: Case Study Eagle Metal Update

to do:
	height

    website - reduced 1650, 1500
    1150-770

    All:
        different sizes

    tablet (900 - 600) specific (860 - 600) 756
        change to desktop version later


        bottom update
            pos lang above
            padding around grub
            what we did space
            photography top

    padding on creative flex
    height element
    collatoral around 600 - font to 1000


    padding on top
    bump images down
    spacing on creative flex
    positioning wide
*/
?>

<?php get_header(); ?>

<link rel="stylesheet" href="/wp-content/themes/drift-2019/css/pages/case-studies/_eagle2.css">

<main class="case-study eagle eagle-update">

    <div class="video-popup">
        <div class="close-it"></div>
        <div class="vid-width">
            <div class="vid-con">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/5cPRApIJqzM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="video-popup-left">
        <div class="close-it"></div>
        <div class="vid-width">
            <div class="vid-con">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/fZ7cjqcrIhY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="video-popup-right">
        <div class="close-it"></div>
        <div class="vid-width">
            <div class="vid-con">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/QfPQk8s0L1Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    
    <section class="s1-eagle case-study-s1 m-400 s1-eagle-update">
        <!-- <p class="superscript">BURGER BAR & KITCHEN</p> -->
        <h1 class="b120">Eagle Metal</h1>
        <p class="gotham">Dallas, Texas</p>
    </section>
	 
	
    <section class="case-study-s2 s2-eagle">
        <div class="img-cover">
            <div class="line line2" data-anchor-target=".img-cover" data-center-top="height:calc(25% + 45px);" data-200-top="height:calc(50% + 82.5px);">
                <div class="play"></div>
            </div>
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/hero-image.jpg" alt="" class="cover desktop-images">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_hero_thumbnail.jpg" alt="" class="cover mobile-images">
        </div>
        <div class="bottom-content">
            <div class="story story-update">
                <h2 class="b40 b40-update">Eagle Metal manufactures connector plates for wood truss construction.</h2>
                <div class="story-content story-content-update">
                    <h3 class="b20 gotham bolder">THE STORY</h3>
                    <p>One of the biggest changes in their industry was in addition to manufacturing plates, competitors were differentiating themselves by also providing a truss design software platform. Truss manufacturers rely on Eagle Metal for the plates, and the software suite nearly runs their whole business.</p>
                </div>
            </div>

                <div class="mobile-list">
                    <h3 class="b20 gotham bolder">WHAT WE DID</h3>
                    <p>Positioning and Strategy</p> 
                    <p>Brand Language</p>
                    <p>Visual Identity</p>
                    <p>Logo Design</p>
                    <p>Software Branding</p>
                    <p>Photoshoots</p> 
                    <p>Brand Video</p>
                    <p>Product Videos</p>
                    <p>Copywriting</p>
                    <p>Marketing Campaigns</p>
                    <p>Print Collateral</p>
                    <p>Social Media Management</p>
                    <p>Tradeshow Booth Design</p>
                </div>

            <div class="what-we-did">
                <h3 class="b20 gotham bolder">WHAT WE DID</h3>
                <ul class="ns">
                    <li>Positioning and Strategy</li> 
                    <li>Brand Language</li>
                    <li>Visual Identity</li>
                    <li>Logo Design</li>
                    <li>Software Branding</li>
                    <li>Photoshoots</li> 
                    <li>Brand Video</li>
                    <li>Product Videos</li>
                    <li>Copywriting</li>
                    <li>Marketing Campaigns</li>
                    <li>Print Collateral</li>
                    <li>Social Media Management</li>
                    <li>Tradeshow Booth Design</li>

                </ul>
            </div> 

            
        </div> 
    </section> 
	
	
	
	
    <div class="case-study-content eagle">
        <div class="menu-anchor-top"></div>
        <div class="side-nav-container sticky" aria-hidden="true">
            <div class="side-nav bottom" aria-hidden="true">
                <ul>
                    <li class=""><a href="#branding"  ><p class="gotham">BRANDING</p></a></li>
                    <li class=""><a href="#website" ><p class="gotham">WEBSITE</p></a></li>
                    <li class=""><a href="#photography" ><p class="gotham">PHOTOGRAPHY</p></a></li>
<!--                     <li class=""><a href="#video" ><p class="gotham">VIDEO</p></a></li>
                    <li class=""><a href="#social" ><p class="gotham">SOCIAL</p></a></li>
                    <li class=""><a href="#results" ><p class="gotham">RESULTS</p></a></li> -->
                </ul>
            </div>
        </div>

        <section class="s3-eagle s3-eagle-update" id="branding">
            <div class="top">
                <h2 class="b173">Branding</h2> 
            </div>
            <div class="center">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/logo.svg" alt="Eagle Metal Logo" />
                <h3><span>Empowering</span> great component manufacturers.</h3>
            </div>
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/branding_img-min.jpg" alt="" class="cover">
        </section>

        <section class="s4-eagle s4-eagle-update">
            <div class="white-box">
                <div class="top">
                    <p class="gotham">The Logo</p>
                    <h3>We started with a manufacturing company, and repositioned them to reflect the dual nature of their business. Part manufacturer. Part Software as a Service.</h3>
                </div>
                <div class="bottom-left">
                    <p class="gotham space">Before</p>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/old-logo.svg" alt="Old Eagle Metal Logo" class="old-logo" />
                </div>
                <div class="bottom-right">
                    <p class="gotham space">After</p>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/new-logo.svg" alt="New Eagle Metal Logo" class="new-logo" />
                </div>
            </div>
            <div class="bottom bottom-update">
                <h3 class="pos-lang">Positioning Language</h3>
                <div class="language-div">
					<div class ="language language-update">
						<div class="javaClass">
							<p><strong>A Genuine Partnership</strong></p>
							<p>Over 30 years in the industry means we understand. We adapt to the needs of every component manufacturer we work with, customizing and simplifying.</p>
						</div>
						<div class="javaClass">
							<p><strong>Innovation that Empowers</strong></p>
							<p>We believe in working smarter, not harder. Engineer solutions so our partners build more efficiently and reduce bottom lines. </p>
						</div>
						<div class="javaClass">
							<p><strong>Bold Craftsmanship</strong></p>
							<p>Our connector products are engineered, tested, and proven right here in the USA. For over 30 years, Eagle Metal has represented a commitment to strength & quality.</p>
						</div>
					</div>	
					
					<div class ="language language-update">
						<div class="javaClass">
							<p><strong>TIME TO BUILD</strong></p>
							<p>This tagline focuses on a call to get to work as well as a nod to the time saved by working with Eagle Metal. We use this hashtag in much of our social media marketing.</p>
						</div>
						<div class="javaClass">
							<p><strong>EMPOWERING GREAT COMPONENT MANUFACTURERS</strong></p>
							<p>This is our primary tagline. Eagle Metal is a resource to help their forward-thinking customer's build more profitably. </p>
						</div>
						<div class="javaClass">
							<p><strong>IT’S TIME FOR AN UPGRADE</strong></p>
							<p>This campaign CTA is used in direct mail marketing. There are two other software giants that are competitors, however they change slowly, and use archaic systems.</p>
						</div>
					</div>
					
                </div>
                <!--<div class="language-mobile">
                    <div>
                        <p><strong>A Genuine Partnership</strong></p>
                        <p>Over 30 years in the industry means we understand. We adapt to the needs of every component manufacturer we work with, customizing and simplifying.</p>
                    </div>
                    <div>
                        <p><strong>Innovation that Empowers</strong></p>
                        <p>We believe in working smarter, not harder. Engineer solutions so our partners build more efficiently and reduce bottom lines. </p>
                    </div>
                    <div>
                        <p><strong>Bold Craftsmanship</strong></p>
                        <p>Our connector products are engineered, tested, and proven right here in the USA. For over 30 years, Eagle Metal has represented a commitment to strength & quality.</p>
                    </div>
                </div>-->

                <h2 class="b173 b173-update">#TimeToBuild</h2>
            </div>
        </section>


        <section class="s5-eagle s5-eagle-update">
            <div class="side">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/upgrade.svg" alt="Engineered. Tested. True." class="engineered engineered-up"/>
            </div>
            <div class="content">
                <div class="left">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/truebuild-suite.svg" alt="TrueBuild Software Suite" class="truebuild-suite" />
                    <p class="leftLine" id="leftLine">We designed a suite of icons for each of the products in the TrueBuild software suite, as well as custom loading screens, and various graphics to support the suite.</p>

                    <p class="leftLine" id="leftLine2">The truebuild brand has it’s own color pallete, to give it it’s own distinction, while still live under the Eagle Metal umbrella.</p>
                </div>
                <div class="right">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/software.svg" alt="TrueBuild Software" class="software" />
                </div>
            </div>
            <!-- <div class="brochure"></div> -->
        </section>

        <section class="mobileTime"> 
            <h4>It’s Time for an Upgrade.</h4>
        </section>

        <section class="s6-eagle s6-eagle-update"> 
            <div class="top s6-top" id="s6-top">
               <!-- <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/bg.png" />-->
                <h2 class="b58 b58-add">We focused the rebrand on building up the target customer, and making them – the truss manufacturer – the hero of the Eagle Metal story.</h2>
            </div>
            <!--<div class="content">
                <div class="left">
                    <div class="tradeshow-slider">
                        <div>
                            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/slide-1.jpg" alt="Tradeshow Booth" />
                </div>
                         <div>
                            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/slide-1.jpg" alt="Tradeshow Booth" />
                        </div>
                        <div>
                            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/slide-1.jpg" alt="Tradeshow Booth" />
                        </div> 
                    </div>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/left-bottom.jpg" alt="Tradeshow Booth" class="tradeshow" />
                </div>
                <div class="right">
                    <div class="text">
                        <h3 class="b40">Tradeshow Booth</h3>
                        <p>The tradeshow booth design is a reflection of their mostly digital brand in a very physical space. We love the physical interaction that happens with tradeshow booth designs.</p>
                    </div>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/right-bottom.jpg" alt="Tradeshow Booth" class="tradeshow" />
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/slide-1.jpg" alt="Tradeshow Booth" class="mobile-tradeshow" />
                </div>
            </div>-->
        </section>
		
		<!--<section class="s6-eagle s6-eagle-update"></section>-->
		
        <section class="s7-eagle">
            <div class="stuff">
               <!-- <h3 class="b40">Splash Screens</h3>-->
                <div class="splash-slider">
                    <div>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img1-min.jpg" alt="TrueBuild Cloud Splash Screen" class="desktop-images"/>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img1.jpg" alt="TrueBuild Cloud Splash Screen" class="mobile-images"/>
                    </div>
                </div>
            </div>
        </section>
        <section class="s8-eagle" id="website">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/website_img-min.jpg" alt="" class="cover">
            <div class="top">
                <h2 class="b173">Website</h2>
            </div>
            <div class="right right-upgrade">
                <h3 class="b40">The website is a showcase for the powerful software tools, and the many ways it can save their customer’s time and expenses.</h3>
                <a href="https://eaglemetal.com" target="_blank" class="main-button">View the website</a>
            </div>
        </section>
		
		
        <section class="s9-eagle s9-eagle-update">
            <div class="top">
                <img class="web-image" src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_homepage_long-min.jpg" alt="Eagle Metal Website Homepage"/>
            </div>
            <div class="center center-update">
                <h3 class="b58">The website is a showcase for the powerful software tools, and the many ways it can save their customer’s time and expenses.
					<br>
					<img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_icon_set.svg" class="img-padding" alt="Eagle Metal Website Interior Pages"/>
				</h3>
            </div>


            <section class="s12-eagle s12-eagle-update">
                <div class="image image-3 conImage noDis2"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img2-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                
                <!-- <div class="phone-slider mobile-images2">
                    <div><img style="width:100%;" src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile.jpg" alt="Eagle Metal Software Use Photo" /></div>
                </div> -->
                
                
                <div class="image image-2 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img3-min.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <!--<div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>-->
                
                <div class="image image-2 mobile-images1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img2.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <!--<div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>-->

                <div class="image image-1 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img4-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                
                <div class="image image-6 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img5-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                <div class="image image-6 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img6-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                <div class="image image-6 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img7-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                <div class="image image-6 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img8-min.jpg" alt="Eagle Metal Construction Photo" /></div>

                
                <div class="image image-1 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img9-min.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <div class="image image-1 mobile-images1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img3.jpg" alt="Eagle Metal Software Use Photo" /></div>

                <div class="image image-2 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img10-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                <div class="image image-2 mobile-images1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img4.jpg" alt="Eagle Metal Construction Photo" /></div>


            <!-- <div class="photography-mobile-slider">
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_thumbnail-min.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img12-min.jpg" alt="Eagle Metal Houses Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img13-min.jpg" alt="Eagle Metal Truss" /></div>
            </div> -->


            </section>
			


            <div class="bottom">
                <img class="desktop-images1 noDis" src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img11-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                <img class="mobile-images1" src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img5.jpg" alt="Eagle Metal Website Interior Pages"/>
            </div>
        </section>
		
        <section class="s10-eagle s10-eagle-update" id="photography">
            <div class="top topTaller">
                <h2 class="b173 video-update">Video & <br> Photography</h2>
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_photography_img-min.jpg" alt="" class="show-it-soon"/>  
            </div>
			
            <!--<div class="bottom">-->
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_photography_img-min.jpg" alt="" class="photo-img hide-it-soon"/>           
			<!--</div>-->
			
			<div class="creativeFlex">
                <h2 class="b173 video-up">Video & <br> Photography</h2>
					<div class="left left-update">
						<h3 class="b40">Creative Direction</h3>
						<p>We use a variety of photographic approaches to showcase the software in different offices, by different users, etc.</p><br/>

						<p>Our photography and video has captured the process of truss fabrication, from the initial manufacturing of the plates, to delivery, and assembling, and finally installation.</p>
					</div>
					<div class="right right-update">
						<p class="sub-text bolder space-bottom">What we did</p>
						<!--<div class="services">
							<div>
								<img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/camera.svg" alt="" class="camera"/>
								<p>Corporate Photography</p>
							</div>
							<div>
								<img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/camera-drone.svg" alt="" class="camera-drone"/>
								<p>Drone Photography</p>
							</div>
							<div>
								<img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/portrait.svg" alt="" class="portrait"/>
								<p>Staff Photography</p>
							</div>
						</div>-->
						
						<p class="">Video Testimonials</p>
						<p class="">Brand Video</p>
						<p class="">Software Highlight Videos</p>
						<p class="">Corporate Photography</p>
						<p class="">Drone Photography</p>
						<p class="">Staff Photography</p>
					</div>
			</div>
       
       
        </section>
		
		
        <section class="s11-eagle s11-eagle-update">
			<div class="shift-up">
				
				
				<div class="smallShift">
					<div class="flexContainer">
						<div>
                            <div class="imageContainerDiv">
							    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_testimonial_thumbnail1.jpg" alt="testimonial"/>
                                
                                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/icon_play.svg" alt="play button" class="testPlayLeft"/>
                            
                            </div>

                            <h3> Lloyd & Sonny </h3>
                            <p> “Service is the only way to differentiate yourself in this industry. And service is our biggest thing.” </p>
                        
                            <h4> LOREDO TRUSS, AUSTIN, TX </h4>
                        </div>
						
						<div>
                            <div class="imageContainerDiv"> 
							    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_testimonial_thumbnail2.jpg" alt="testimonial"/>
                                

                                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/icon_play.svg" alt="play button" class="testPlayRight"/>
                            
                            </div>
                            
                            <h3> Steve & Mark </h3>
                            <p> “the nice thing about Eagle Metal is that they are truss people...” </p>

                            <h4> TRUSPRO, GUADALUPE, CA </h4>
                        </div>
					</div>
				</div>
			</div>
			
			
			
        </section>
		
        <!--<section class="s12-eagle">
            <div class="image image-1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/construction.jpg" alt="Eagle Metal Construction Photo" /></div>
            <div class="image image-2"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/computer.jpg" alt="Eagle Metal Software Use Photo" /></div>
            <div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>
            <div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/eagle-photo-1.jpg" alt="Eagle Metal Truss" /></div>

            <div class="photography-mobile-slider">
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/computer.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/eagle-photo-1.jpg" alt="Eagle Metal Truss" /></div>
            </div>

            <h3 class="b40 mobile-statement">Statement about photography. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed </h3>

        </section>-->





        <section class="s9-eagle s9-eagle-update">
            <section class="s12-eagle s12-eagle-update">
                <div class="image-2 videoContainer">
                    <div class="playButton"><p> PLAY </p></div>
                    <div class="image desktop-images"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_thumbnail-min.jpg" alt=""></div>
                    <!--<div class="image mobile-images"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img6.jpg" alt=""></div>
                --></div>
                <!--<div class="image image-2"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_thumbnail-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                --><div class="image image-1 noDis"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img12-min.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <div class="image image-1 mobile-images1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img7.jpg" alt="Eagle Metal Software Use Photo" /></div>
            </section>


            <div class="bottom marginBottom">

                <div class="photography-mobile-slider">
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img13-min.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_thumbnail-min.jpg" alt="Eagle Metal Software Use Photo" /></div>
                </div>

                <div class="photography-slider desktop-images">
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img13-min.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/slider_img1.jpg" alt="Eagle Metal Software Use Photo" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/slider_img2.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/slider_img4.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/slider_img5.jpg" alt="Eagle Metal Software Use Photo" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/slider_img6.jpg" alt="Eagle Metal Truss" /></div>
                </div>

                <div class="photography-slider mobile-images">
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery1.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery2.jpg" alt="Eagle Metal Software Use Photo" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery3.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery4.jpg" alt="Eagle Metal Truss" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery5.jpg" alt="Eagle Metal Software Use Photo" /></div>
                    <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_gallery6.jpg" alt="Eagle Metal Truss" /></div>
                </div>

                <!--<div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img13-min.jpg" alt="Eagle Metal Truss" /></div>-->
                
                
            
            </div>
        </section>





        <section class="s10-eagle differentPad s10-eagle-update-2" id="photography">
            <div class="top topNew">
                <h2 class="b173 coll-update">Collateral</h2>
            </div>
			
            <!--<div class="bottom">-->
                <!--<img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_photography_img-min.jpg" alt="" />     -->      
			<!--</div>-->
			
			
        </section>

        <section class="s9-eagle s9-eagle-update s9-blue">
            
            <!--<div class="top">
                <h2 class="b173">Collateral</h2>
            </div>-->

            <section class="s12-eagle s12-eagle-update">
                <div class="image image-4"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img14-min.jpg" alt="Eagle Metal Construction Photo" /></div>
                <div class="image image-4"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img15-min.jpg" alt="Eagle Metal Software Use Photo" /></div>


            


            </section>


            <div class="bottom">
                <div class="image image-3 desktop-images1 morepadding-top"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img16-min.jpg" alt="Eagle Metal Truss" /></div>
                <div class="image image-3 mobile-images1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle-mobile/eagle_mobile_img10.jpg" alt="Eagle Metal Truss" /></div>
                
            </div>
            
        </section>


        <section class="s9-eagle s9-eagle-update pad-top">
            <section class="s12-eagle s12-eagle-update">
                <div class="image image-2"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img17-min.jpg" alt="Eagle Metal Truss" /></div>
                <div class="image image-1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img18-min.jpg" alt="Eagle Metal Truss" /></div>
            </section>
        </section>
            
        <section class="s13-eagle">
            <div class="line"></div>
            <div class="text">
                <p class="sub-text bolder">More work this way</p>
                <a href="/grub" class="dot-link">Grub</a>
            </div>
        </section>

        <div class="menu-anchor-end"></div>
    </div>
</main>
<script>
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".menu-anchor-end").offset().top;
        var div_top = $('.menu-anchor-top').offset().top;
        var div_height = $(".sticky").height();
        var padding = 0;  // tweak here or get from margins etc
        
        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({top: (window_top + div_height - footer_top + padding) * -1})
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.sticky').css({top:0})
        } else {
            $('.sticky').removeClass('stick');
        }
        }
        $(function () {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
            
        });



        function onResizeReadMoreLayout(){
            if ($(window).width() >= 2300) {
                $('.web-image').addClass('reduced1');
                $('.creativeFlex').addClass('increased-padding');
                $('.shift-up').addClass('shift-higher');
                $('.video-update').addClass('video-shifted');
            }
            else if ($(window).width() >= 2000) {
                $('.web-image').addClass('reduced2');
                $('.video-update').addClass('video-normal');
            }
            else{
                $('.web-image').removeClass('reduced1');
                $('.web-image').removeClass('reduced2');
                $('.creativeFlex').removeClass('increased-padding');
                $('.shift-up').removeClass('shift-higher');
                $('.video-update').addClass('video-normal');
                $('.video-update').addClass('video-shifted');
            }  
        }

    // initial state
    onResizeReadMoreLayout();
    // on resize
    $(window).on('resize', onResizeReadMoreLayout);

//javaClass
function onResizeReadMoreLayout1(){
            if ($(window).width() <= 900) {
                $('.javaClass').addClass('language-bottom');
                $('.pos-lang').addClass('margin-change');
                $('.b58-add').addClass('wider-class');
                console.log("true");
                //$('.s6-top').setAttribute('style', 'padding: 10px');
                document.getElementById("s6-top").style.padding = "10px 0px 100px 0px";
                $('.leftLine').addClass('leftAlign');
            }
            else if($(window).width() <= 600){
                $('.javaClass').removeClass('language-bottom');
                $('.pos-lang').removeClass('margin-change');
                //document.getElementById("s6-top").style.padding-top = "10px !important";
                $('.b58-add').removeClass('wider-class');
            }  
        }

    // initial state
    onResizeReadMoreLayout1();
    // on resize
    $(window).on('resize', onResizeReadMoreLayout1);




    $( document ).ready(function() {
        $('.splash-slider').slick({
            slidesToShow: 1
        });  
        $('.photography-mobile-slider').slick({
            slidesToShow: 1
        }); 
        $('.language-mobile').slick({
            slidesToShow: 1
        }); 
        $('.photography-slider').slick({
            slidesToShow: 1
        });
        $('.phone-slider').slick({
            slidesToShow: 1
        });
    });

    $(".play").click(function() {
        $(".video-popup").addClass("active");
        $("body").addClass("no-scroll");
    });

    $(".playButton").click(function() {
        $(".video-popup").addClass("active");
        $("body").addClass("no-scroll");
    });

    $(".testPlayLeft").click(function() {
        $(".video-popup-left").addClass("active");
        $("body").addClass("no-scroll");
    });

    $(".testPlayRight").click(function() {
        $(".video-popup-right").addClass("active");
        $("body").addClass("no-scroll");
    });
    

    $(".close-it").click(function() {
        $(".active").removeClass("active");
        $("body").removeClass("no-scroll");
    });
</script>
<?php get_footer(); ?>