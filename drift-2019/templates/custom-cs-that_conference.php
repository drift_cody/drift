<?php
/*
Template Name: Case Study That Conference
*/
?>

<?php get_header(); ?>
<main class="case-study that-conference">
    <div class="video-popup">
        <div class="close-it"></div>
        <div class="video-container">
            <div class="video">
                <div class="iframe-container">
                    <div id="player"></div>
                </div>
            </div>
        </div>
    </div>
    <section class="s1-that">
        <div class="page-title">
            <h1 class="b120 page-title__header">That Conference</h1>
            <p class="page-title__superscript">A PREMIERE TECH CONFERENCE IN WISCONSIN</p>
            <div class="page-title__line"></div>
        </div>
    </section>
    <section class="s2-that">
        <div class="row-images row-images__single">
            <div>
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-conference-min.jpg"
                    alt="" class="row-images__image">
            </div>
        </div>
        <!-- <div class="img-cover">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-conference-min.jpg" alt="" class="cover">
        </div> -->
    </section>
    <section class="s3-that">
        <div class="side-character">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-green-bear.svg"
                alt="Green Bear">
        </div>
        <div class="cs-story">
            <div class="cs-story__top">
                <h2 class="b40 cs-story__header">We helped That Conference develop a brand narrative centered on the
                    idea that a tech conference could be just like a “summer camp for geeks.” </h2>
                <div class="cs-story__story">
                    <p>THE STORY</p>
                    <p>Our branding approach created opportunities for language that felt more like a campfire
                        conversation. And for imagery and illustrations that subtly nodded to vintage children story
                        books. Every new year is a family reunion. It’s a chance to share stories and embrace a
                        child-like curiosity for all things technology.
                    </p>
                    <p>That Conference is located in Wisconsin Dells, WI and it’s at a giant indoor water park. Over
                        1700 people attended last year and the brand is continuing to grow. Incredible speakers and
                        workshops are lined up between pig roasts, board game nights, and water park parties. The spirit
                        that we helped develop has created a culture of social exploration and playful learning.</p>
                </div>
            </div>
            <p class="cs-story__list-header">WHAT WE DID</p>
            <ul class="cs-story__list">
                <li class="cs-story__item">Brand Strategy</li>
                <li class="cs-story__item">Brand Messaging</li>
                <li class="cs-story__item">Tagline Development</li>
                <li class="cs-story__item">Conference Branding</li>
                <li class="cs-story__item">Brand Videography</li>
                <li class="cs-story__item">Conference Web Design</li>
                <li class="cs-story__item">Environmental Graphics</li>
                <li class="cs-story__item">Digital Marketing</li>
                <li class="cs-story__item">Signage</li>
            </ul>
        </div>
    </section>
    <section class="s4-that">

        <div class="img-cover">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-conference-lunch-min.jpg"
                alt="" class="cover">
        </div>
    </section>
    <section class="s5-that">

        <div class="s5-content">
            <div class="cs-logo-compare">
                <div class="cs-logo-compare__top">
                    <h2 class="b173 cs-logo-compare__top-text">2020<br />Branding</h2>
                    <div class="cs-logo-compare__colors">
                        <img class="desktop"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-colors.svg"
                            alt="Color Pallett">
                        <img class="mobile"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-colors-mobile.svg"
                            alt="Color Pallett">
                    </div>
                </div>
                <div class="cs-logo-compare__center">
                    <div class="cs-logo-compare__logo-container">
                        <p class="cs-logo-compare__logo-title">PRIMARY LOGO</p>
                        <div class="cs-logo-compare__logo">
                            <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-conference-logo.svg"
                                alt="That conference logo">
                        </div>
                    </div>
                    <div class="cs-logo-compare__logo-container">
                        <p class="cs-logo-compare__logo-title">SECONDARY LOGO</p>
                        <div class="cs-logo-compare__logo">
                            <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-us-logo.svg"
                                alt="That.US Logo">
                        </div>
                    </div>
                </div>
                <div class="cs-logo-compare__bottom">
                    <p class="cs-logo-compare__bottom-title">CHARACTER ILLUSTRATIONS</p>
                    <div class="cs-logo-compare__character">
                        <img id="racoon"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-racoon.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="moose"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-moose.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="robot"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-robot.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="orange-bear"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-orange-bear.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="green-bear"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-green-bear.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="pink-bear"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-pink-bear.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="bigfoot"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-big-foot.svg"
                            alt="That.US Logo">
                    </div>
                    <div class="cs-logo-compare__character">
                        <img id="octo"
                            src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/character-octopus.svg"
                            alt="That.US Logo">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="s6-that block-h-3col">
        <div class="-row">
            <h2 class="b40 block-h-3col__heading">What makes THAT Conference so awesome</h2>
            <div class="block-h-3col__columns">
                <div class="block-h-3col__column">
                    <h3 class="b20 block-h-3col__column-heading">Zero - 1700 person tech conference</h3>
                    <p class="b18 block-h-3col__column-text">We were involved at the very beginning, and had a defining
                        role in developing the strategy that shaped the culture and personality of That Conference.</p>
                </div>
                <div class="block-h-3col__column">
                    <h3 class="b20 block-h-3col__column-heading">40% Returning Campers YoY</h3>
                    <p class="b18 block-h-3col__column-text">The culture and brand embrace community and family. And
                        that’s why people keep coming back.</p>
                </div>
                <!-- <div class="block-h-3col__column">
                    <h3 class="b20 block-h-3col__column-heading">A digital spark</h3>
                    <p class="b18 block-h-3col__column-text">Leveraging the community-focused mission, we helped create
                        a digital experience that helps campers connect, and extend the That Conference brand across 365
                        days. The UI/UX was built for conversion and conversation, with clear call to actions and plenty
                        of quirky illustrations.</p>
                </div> -->
            </div>
        </div>
    </section>
    <section class="s7-that">
        <div class="c-img-overlay">
            <div class="c-img-overlay__icon icon-that-con"></div>
            <h2 class="b58 c-img-overlay__text">In 2020 we reimagined the That Conference digital experience.</h2>
        </div>
    </section>
    <section class="s8-that">
        <h2 class="b173">Website</h2>

        <div class="c-scrolling-text ">
            <div class="sticky-anchor"></div>
            <div class="sticky c-scrolling-text__text-container">
                <p class="b40 c-scrolling-text__header">A digital spark</p>
                <p class="b18 c-scrolling-text__text">Leveraging the community-focused mission, we helped create
                    a digital experience that helps campers connect, and extend the That Conference brand across 365
                    days. The UI/UX was built for conversion and conversation, with clear call to actions and plenty
                    of quirky illustrations.</p>
            </div>
            <div class="c-scrolling-text__image">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-homepage-min.jpg"
                    alt="Website Preview" />
            </div>
            <div class="scrolling-text-end"></div>
        </div>

    </section>
    <section class="s9-that">
        <div class="-row">
            <div class="images-row">
                <div class="images-row__image-container">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-laptop-min.jpg"
                        class="images-row__image">
                </div>
                <div class="images-row__image-container">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-phone-min.jpg"
                        class="images-row__image">
                </div>
            </div>
        </div>
        <div class="-row">
            <div class="images-row">
                <div class="images-row__image-container">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-hero-image2-min.jpg"
                        class="images-row__image">
                </div>
            </div>
        </div>
    </section>
    <section class="s10-that">
        <div class="page-title">
            <p class="page-title__superscript">BRAND VIDEO</p>
            <h2 class="b40 page-title__header">See what all the hype is about!</h2>
        </div>
        <div class="-row">

            <div class="images-row">

                <div class="images-row__image-container s10-that-video">
                    <div class="line">
                        <div class="play"></div>
                    </div>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-highlight-vid-min.jpg"
                        class="images-row__image">
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="s11-that">
        <h2 class="b173">Past Conferences</h2>
        <div class="cs-logo-compare">
            <div class="cs-logo-compare__top">
                <h2 class="b40 cs-logo-compare__top-text">The evolution of the logo. Lorem ipsum dolor sit amet,
                    consetetur sadipscing elitr, sed diam nonumy eirmod .</h2>
            </div>
            <div class="cs-logo-compare__center">
                <div class="cs-logo-compare__logo-container">
                    <p class="cs-logo-compare__logo-title">2012</p>
                    <div class="cs-logo-compare__logo">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-2012-logo.svg"
                            alt="That conference logo">
                    </div>
                </div>
                <div class="cs-logo-compare__logo-container">
                    <p class="cs-logo-compare__logo-title">2014</p>
                    <div class="cs-logo-compare__logo">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-2014-logo.svg"
                            alt="That.US Logo">
                    </div>
                </div>
            </div>
            <div class="cs-logo-compare__center">
                <div class="cs-logo-compare__logo-container">
                    <p class="cs-logo-compare__logo-title">2016</p>
                    <div class="cs-logo-compare__logo">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-2016-logo.svg"
                            alt="That conference logo">
                    </div>
                </div>
                <div class="cs-logo-compare__logo-container">
                    <p class="cs-logo-compare__logo-title">2019</p>
                    <div class="cs-logo-compare__logo">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-us-logo.svg"
                            alt="That.US Logo">
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section class="s12-that">
        <div class="s12-that__content">
            <h2 class="b40 s12-that__header">Meet Walter. Over the years, he’s evolved into the biggest That Conference
                fan.</h2>
            <p class="b18 s12-that__text">All year long it’s all he can talk about. “This one time at That Conference”
                is getting to be so
                old. We love ‘em, but it gets exhausting.</p>
        </div>
        <div class="s12-that__characters">
            <div class="s12-that_character">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-brown-bear.svg"
                    alt="">
                <p class="s12-that__character-year">2012</p>
            </div>
            <div class="s12-that_character">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-bear-head.svg"
                    alt="">
                <p class="s12-that__character-year">2014</p>
            </div>
            <div class="s12-that_character">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-business-bear.svg"
                    alt="">
                <p class="s12-that__character-year">2016</p>
            </div>
            <div class="s12-that_character">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-counselor-bear.svg"
                    alt="">
                <p class="s12-that__character-year">2017</p>
            </div>
            <div class="s12-that_character">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/tc-orange-bear.svg"
                    alt="">
                <p class="s12-that__character-year s12-that__character-year--right">2019</p>
            </div>
        </div>
    </section>
    <section class="s13-that">
        <div class="c-slideshow-caption content">
            <div class="c-slideshow-caption_images base-slideshow">
                <div class="c-slideshow-caption_buttons base-slideshow_buttons">
                    <div
                        class="base-slideshow_button base-slideshow_button_blue c-slideshow-caption_buttons-prev icon-arrow-nc">
                    </div>
                    <div
                        class="base-slideshow_button base-slideshow_button_blue c-slideshow-caption_buttons-next icon-arrow-nc">
                    </div>
                </div>
                <div class="c-slideshow-caption_slick-slides">

                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/slideshow-illustration/that-ss-1.jpg"
                        alt="">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/slideshow-illustration/that-ss-1.jpg"
                        alt="">
                </div>
            </div>
            <div class="c-slideshow-caption_content">
                <div class="c-slideshow-caption_header">
                    <h2>
                        <span class="stroke-text b120">2017-2019</span>
                        <span class="b40">Illustration Examples</span>
                    </h2>
                </div>
                <div class="c-slideshow-caption_slick-text">

                    <div class="text-slider-container">
                        <div class="c-slideshow-caption_slick-text-item">
                            <p class="b20">CREATIVE DIRECTION</p>
                            <p class="b18">Illustration has been a powerful visual element of the That Conference
                                narrative. The bear, and other creatures, have been a reminder to embrace that
                                child-like curiosity for learning at every opportunity </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="s14-that">
        <h2 class="b58">The visual identity is carried out across a variety of digital, spatial, and physical objects to
            create an immersive, unparalleled experience.</h2>
    </section>
    <section class="s15-that">
        <div class="-row">
            <div class="images-row">
                <div class="images-row__image-container">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-bc.jpg"
                        class="images-row__image">
                </div>
                <div class="images-row__image-container">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-flyer.jpg"
                        class="images-row__image">
                </div>
            </div>
        </div>
        <div class="-row">
            <!-- <div class="base-slideshow">
                <div class="base-slideshow_buttons">
                    <div class="s15-slideshow-prev base-slideshow_button base-slideshow_button_blue icon-arrow-nc">
                    </div>
                    <div class="s15-slideshow-next base-slideshow_button base-slideshow_button_blue icon-arrow-nc">
                    </div>
                </div>
                <div class="base-slideshow_images s15-that-slideshow">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-badge.jpg"
                        class="images-row__image">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/that-conference/that-badge.jpg"
                        class="images-row__image">
                </div>
            </div> -->
        </div>
    </section>
    <section class="s16-that last">
        <div class="line"></div>
        <p class="gotham">ON TO THE NEXT ONE</p>
        <div class="next">
            <a href="/work/george-bush-library/" class="dot-link next">George Bush Library</a>
            <!-- <a href="<?php bloginfo('template_url'); ?>/that-conference" class="dot-link next">That Conference</a> -->
        </div>
    </section>
</main>

<script>
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '600',
            width: '1000',
            videoId: 'R67gLABGvVE',
            playerVars: {
                rel: 0,
                loop: 1
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady() {
        $(".play").click(function () {
            player.playVideo();
        });
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            //setTimeout(stopVideo, 6000);
            done = true;
        }
    }

    function stopVideo() {
        player.stopVideo();
    }
    // $('header').addClass('fade-in');

    $(".play").click(function () {
        $(".video-popup").addClass("active");
        $("body").addClass("no-scroll");
    });

    $(".close-it").click(function () {
        $(".video-popup.active").removeClass("active");
        $("body").removeClass("no-scroll");
        stopVideo();
    });
    $(document).ready(function () {



        $('.c-slideshow-caption_slick-slides').slick({
            dots: false,
            prevArrow: $('.c-slideshow-caption_buttons-prev'),
            nextArrow: $('.c-slideshow-caption_buttons-next'),
            asNavFor: '.text-slider-container'
        });
        $('.text-slider-container').slick({
            fade: true,
            dots: false,
            arrows: false
        });
        $('.s15-that-slideshow').slick({
            dots: false,
            prevArrow: $('.s15-slideshow-prev'),
            nextArrow: $('.s15-slideshow-next'),
            asNavFor: '.text-slider-container'
        });
    });

    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        //console.log('window top: '+window_top);
        var footer_top = $(".scrolling-text-end").offset().top;
        //console.log('footer top: '+footer_top);
        var div_top = $('.sticky-anchor').offset().top;
        // console.log('div top: '+div_top);
        var div_height = $(".sticky").height();
        //console.log('div height: '+div_height);


        var padding = 180; // tweak here or get from margins etc

        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({
                top: (window_top + div_height - footer_top + padding) * -1
            })
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.sticky').css({
                top: 0
            });

        } else {
            $('.sticky').removeClass('stick');
        }
    }

    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });
</script>
<?php get_footer(); ?>