<?php
/*
Template Name: Careers
*/
?>

<?php get_header(); ?>


<main class="careers">
    <section class=" s1_career g-side-by-side-header -row -row--inset">
        <div class="left">
            <h1 class="b100 lapture lapture__bold"><?php the_field('s1_left_text'); ?></h1>
        </div>
        <div class="right">
            <p class="b28 lapture"> <?php the_field('s1_right_text') ?> </p>
        </div>
    </section>
    <section class="career-image">
        <div class="row-images">
            <div class="">
                <img src="<?php echo esc_url(get_field('header_photo')['url']) ?>" alt=""  class="row-images-row__image"></img>
            </div>
        </div>
    </section>
    <section class="s2_career -row  -row--inset">
        <div class="left">
            <h2 class="lapture lapture__bold b58"> <?php the_field('s2_left_text'); ?> </h2>
            <a href="<?php echo esc_url(get_field('s2_link')['url']); ?>" class="main-button"><?php echo get_field('s2_link')['title']; ?></a>
        </div>
        <div class="right">
            <p class="gotham b18"> <?php the_field('s2_right_text'); ?></p>
            <?php the_field('s2_right_list'); ?>
        </div>
    </section>

    <section class="s3_career u-pad -row m-100">
        <?php get_template_part('template', 'career'); ?>
    </section>

    <section class="s4_career core-values__section u-pad m-100">
        <?php if (have_rows('core_values')) :
            while (have_rows('core_values')) : the_row(); ?>


                <div class="core-values u-pad-small -row">
                    <div class="core-values__text ">
                        <?php the_sub_field('left_text'); ?>
                        <a href="<?php echo esc_url(get_sub_field('link')['url']); ?>" class="main-button main-button__white"> <?php echo get_sub_field('link')['title']; ?></a>
                    </div>
                    <div class="core-values__items">
                        <?php $i = 01;
                                if (have_rows('right_text')) :
                                    while (have_rows('right_text')) : the_row();  ?>
                                <div class="core-values__item">
                                    <h2 class=" b40"><span>0<?php echo $i ?></span><?php echo the_sub_field('heading') ?></h2>
                                    <p class="b18 gotham"> <?php echo the_sub_field('text') ?></p>
                                </div>
                        <?php $i++;
                                    endwhile;
                                endif; ?>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>
    </section>
    <section class="s5_career m-300">

        <?php if (have_rows('s5_images')) : ?>
            <?php while (have_rows('s5_images')) : the_row(); ?>
            <div class="-row">
                <div class="images-row">
                    <?php if (have_rows('image_row')) : ?>
                        <?php while (have_rows('image_row')) : the_row(); ?>
                            <div class="images-row__image-container">
                                <img class="images-row__image" src="<?php echo esc_url(get_sub_field('image')['url']); ?>" alt="">
                            </div>
                    <?php endwhile;
                            endif; ?>
                </div>
            </div>
        <?php endwhile;
        endif; ?>

    </section>
    <section class="s6_career -row u-pad-small">
        <?php $job_count = get_category('48');
        $job_total_num = $job_count->count; ?>
        <?php if ($job_total_num > 0) : ?>
            <h2 class="b58 lapture__bold"><?php the_field('header'); ?></h2>
                <div class="line-group">
                <div class="line"></div>
                <div class="circle icon-lightning-bolt"></div>
            </div>
            <div class="job-postings">
            <?php
                $args = array(
                    'post_type' => 'post',
                    'cat' => 48,
                    'order' => 'ASC'
                );
                $loop = new WP_Query($args);
                while($loop->have_posts()) : $loop->the_post();
                    $alt=get_post_thumbnail_id(); $alt = get_post_meta($alt, '_wp_attachment_image_alt', true);
            ?>
                <div class="job-posting__job">
                    <div class="img-cover">
                        <a href="<?php the_permalink(); ?>" class="job-posting__link"></a><img src="<?php echo get_the_post_thumbnail_url();?>" alt="<?php echo $alt ?>" class="job-posting__img cover">
                    </div>
                    <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                    <p class="date"><?php the_time('F j, Y'); ?></p>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
            </div>
        <?php else : ?>
            <h2 class="b58 lapture__bold">Curious about openings? Email us.</h2>
                <div class="line-group">
                <div class="line"></div>
                <div class="circle icon-lightning-bolt"></div>
            </div>
        <?php endif; ?>
        
    </section>
    <section class="s7_career m-150">
        <div class="-row">
            <div class="image-row">
                <div class="image">
                    <img src="<?php echo esc_url(get_field('s7_left_image')['url']); ?>" alt="">
                </div>
                <div class="image">
                    <img src="<?php echo esc_url(get_field('s7_right_image')['url']); ?>" alt="">
                </div>
            </div>
        </div>
        <div class="-row">
            <div class="video-row-career ">
                <div class="video">
                    <?php if (get_field('s7_video_type') == 'Upload') : ?>
                        <div class="video-container">
                            <video autoplay muted loop>
                                <source src="<?php the_sub_field('video_upload'); ?>">
                            </video>
                        </div>
                    <?php endif; ?>
                    <?php if (get_field('s7_video_type') == 'YouTube') : ?>
                        <div class="video-container youtube">
                            <img src="<?php echo esc_url(get_field('s7_thumbnail')['url']); ?>" alt="">
                            <div class="play"></div>
                        </div>
                        <div class="video-popup">
                            <button class="close-it"></button>
                            <div class="vid-width">
                                <div class="vid-con">
                                    <?php the_field('s7_youtube_embed'); ?>       
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="image">
                    <img src="<?php echo esc_url(get_field('s7_bottom_image')['url']); ?>" />
            </div>
        </div>
    </section>

    <section class="newsletter m-100">
        <div class="newsletter__circle icon-envelope_solid"></div>
        <div class="newsletter__content">
            <div class="newsletter__content__left">
                <p class="p-18">UPDATES AND INSIGHTS – NOT SPAM</p>
                <h2 class="b58">Keep your inbox fresh with our Drift updates.</h2>
            </div>
            <div class="newsletter__content__right">
                <!-- Begin Mailchimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://driftingcreatives.us12.list-manage.com/subscribe/post?u=cbe005d9830b5e01dbd327281&amp;id=de707a89a1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">

                            <div class="mc-field-group">
                                <label for="mce-EMAIL">What's your email address?<span>*</span></label>
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="hello@driftingcreatives.com">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cbe005d9830b5e01dbd327281_de707a89a1" tabindex="-1" value=""></div>
                            <div class="clear icon-arrow-nc"><input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button "></div>
                        </div>
                    </form>
                </div>

                <!--End mc_embed_signup-->
            </div>
        </div>
        <div class="newsletter__image img-cover u-margin">
            <img src="<?php echo esc_url(get_field('s8_image')['url']); ?>" alt="<?php echo esc_attr(get_field('s8_image')['alt']); ?>" class="cover">
        </div>
        <!-- <div class="row-images">
            <div>
                <img src="<?php echo esc_url(get_field('s8_image')['url']); ?>" alt="<?php echo esc_attr(get_field('s8_image')['alt']); ?>" class="row-images__image">
            </div>
        </div> -->
    </section>
    <section class="s4-work m-200">
        <div class="line"></div>
        <p class="gotham">WE'D LOVE TO HEAR FROM YOU</p>
        <p class="next"><a class="dot-link" href="<?php echo esc_url(get_field('bottom_link')['url']) ?>"><?php echo esc_html(get_field('bottom_link')['title']) ?></a></p>
    </section>
</main>

<script>
    $('.vid-con iframe').addClass('youtube-video');


	$(".video-container").click(function() {
		$(this).siblings().addClass("active");
		$("body").addClass("no-scroll");
	});

	$(".close-it").click(function() {
        console.log('clicked');
		$(".video-popup.active").removeClass("active");
		$("body").removeClass("no-scroll");
		$('.youtube-video').each(function(){
            this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
        });
	});

</script>
<?php get_footer(); ?>