<?php
/*
Template Name: Mailing List Subscribe
*/
?>

<?php get_header(); ?>

<main class="mailing-list">

	<?php if (get_field('type') == 'video') { ?>
		<div class="video-popup sub-success">
			<div class="close-it"></div>
			<div class="video-container">
				<div class="video">
					<div class="iframe-container">
						<?php the_field('embed_code'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>


	<section class="s1-mailing-list">
		<?php the_title('<h1 class="b58">','</h1>'); ?>
		<div class="img-cover">
			<?php
				$image = get_field('s1_image');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)
				if( $image ) {
				    echo wp_get_attachment_image( $image, $size );
				}
			?>
		</div>
	</section>
	<section class="s2-mailing-list">
		<div class="left">
			<?php the_field('s2_left_text'); ?>
		</div>
		<div class="right">
			<div class="form">
				<div class="text"><?php the_field('s2_right_text'); ?></div>
				<p>What's your email address?<span>*</span></p>
				<form class="form-inline">
			        <div class="form-group mb-2">
			            <label for="email" class="sr-only"><?php _e('Email'); ?></label>
			            <input type="email" class="form-control-plaintext" id="email" placeholder="hello@driftingcreatives.com">
			        </div>
			        <button type="submit" class="btn btn-primary mb-2 subscribe"><?php _e('Subscribe'); ?></button>
			    </form>
			    <div class="error-message"></div>
			</div>
			<?php if (get_field('type') == 'pdf') { ?>
				<div class="download-button sub-success">
					<p>Thanks for subscribing. If you like what you read, be sure to follow us on <a href="https://www.instagram.com/driftcreate" target="_blank">instagram</a>, <a href="https://www.linkedin.com/company/drifting-creatives" target="_blank">linkedin</a>, or <a href="https://www.facebook.com/DriftingCreatives/" target="_blank">facebook</a>.</p>
					<a href="<?php the_field('file'); ?>" class="main-button" target="_blank" download>Download</a>
				</div>
			<?php } ?>
			<?php if (get_field('type') == 'video') { ?>
				<div class="download-button sub-success">
					<p>Thanks for subscribing. If you liked what you saw, be sure to follow us on <a href="https://www.instagram.com/driftcreate" target="_blank">instagram</a>, <a href="https://www.linkedin.com/company/drifting-creatives" target="_blank">linkedin</a>, or <a href="https://www.facebook.com/DriftingCreatives/" target="_blank">facebook</a>.</p>
				</div>
			<?php } ?>
		</div>
	</section>
</main>

<script>
	$(".close-it").click(function() {
		$(".video-popup.active").removeClass("active");
	});
</script>

<?php get_footer(); ?>