<?php
/*
Template Name: Origin
*/
?>

<?php get_header(); ?>

<main class="origin-story">
    
    <section class="s1-origin">
        <?php the_field('header_text'); ?>
        <p class="gotham">FOLLOW US THROUGH THE YEARS</p>
        <div class="line-group">
            <div class="circle icon-lightning-bolt"></div>
            <div class="line"></div>
        </div>
    </section>
    <section class="s3-work">
    <div class="sticky-anchor"></div>
    <div class="year-nav sticky">
        <ul class="ns">
        <?php if( have_rows('years') ): $i = 1;?>
	    <?php while( have_rows('years') ): the_row(); ?>
        <?php if($i === 1){ ?>
            <li class="line-hover gotham year-li<?php echo the_sub_field('year'); ?> active" id="year-<?php echo $i; ?>"><?php echo the_sub_field('year') ?></li>
        <?php }else{ ?>
            <li class="line-hover gotham year-li<?php echo the_sub_field('year'); ?>" id="year-<?php echo $i; ?>"><?php echo the_sub_field('year') ?></li>
        <?php }?>
        <?php $i++;  endwhile; endif; ?>
        </ul>
    </div>
    <!-- Each year will get this chunck of code -->
    <?php if( have_rows('years') ): $i = 1; ?>
	<?php while( have_rows('years') ): the_row();?>
    <?php if($i === 1){ ?>
        <div class="year year-<?php echo $i ?> active">
    <?php }else{ ?>
        <div class="year year-<?php echo $i ?>">
    <?php } ?>
        <div class="top">
            <div class="content">
                <p class="lapture year"><?php the_sub_field('year');?></p>
                <?php the_sub_field('top_text');?>
            </div>
        </div>
        <div class="bottom">
            <?php get_template_part( 'template', 'single-work' ); ?>
        </div>
    </div>
    <?php $i++; endwhile; endif; ?>
	<div class="scrolling-nav-end"></div>
	</section>
</main>

<script>
    function sticky_relocate() {
	    var window_top = $(window).scrollTop();
	    var footer_top = $(".scrolling-nav-end").offset().top;
        var div_top = $('.sticky-anchor').offset().top;
	    var div_height = $(".sticky").height();

	    var padding = 30;  // tweak here or get from margins etc
	    
	    if (window_top + div_height > footer_top - padding)
	        $('.sticky').css({top: (window_top + div_height - footer_top + padding) * -1})
	    else if (window_top > div_top) {
	        $('.sticky').addClass('stick');
	    } else {
            $('.sticky').removeClass('stick');
	        $('.sticky').css({top: 60})
	    }
	}

	$(function () {
	    $(window).scroll(sticky_relocate);
	    sticky_relocate();
	});
</script>
<?php get_footer(); ?>