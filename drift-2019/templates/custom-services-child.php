<?php
/*
Template Name: Services Child
*/
?>

<?php get_header(); ?>

<main class="services-child">
    <section class="s1-services-child m-300">
        <div class="content">
            <div class="left">
                <?php the_field('header_text'); ?>
            </div>
            <p class="p-25"><?php the_field('side_text'); ?></p>
        </div>
        <div class="img-cover">
            <img src="<?php echo esc_url(get_field('s1_image')['url']); ?>" alt="<?php echo esc_attr(get_field('s1_image')['alt']); ?>" class="cover">
        </div>
    </section>
    <section class="s2-services-child m-230">
        <div class="content">
            <div class="left">
                <h2 class="b58"><?php the_field('s2_side_text'); ?></h2>
            </div>
            <div class="right">
                <?php the_field('s2_text'); ?>

            </div>
        </div>
    </section>

    <div class="newsletter m-100">
        <div class="newsletter__circle icon-envelope_solid"></div>
        <div class="newsletter__content u-pad">
            <div class="newsletter__content__left">
                <p class="p-25">SIGN UP FOR OUR NEWSLETTER</p>
                <h2 class="b58">Learn from the Drift team straight from your inbox!</h2>
            </div>
            <div class="newsletter__content__right">
                <!-- Begin Mailchimp Signup Form -->
                <div id="mc_embed_signup">
                    <form action="https://driftingcreatives.us12.list-manage.com/subscribe/post?u=cbe005d9830b5e01dbd327281&amp;id=de707a89a1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">

                            <div class="mc-field-group">
                                <label for="mce-EMAIL">What's your email address?<span>*</span></label>
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="hello@driftingcreatives.com">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cbe005d9830b5e01dbd327281_de707a89a1" tabindex="-1" value=""></div>
                            <div class="clear icon-arrow-nc"><input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button "></div>
                        </div>
                    </form>
                </div>

                <!--End mc_embed_signup-->
                <!-- <div class="input">
                    <label for="email">Whats your email address?<span>*</span></label>
                    <input type="email" placeholder="hello@driftingcreatives.com">
                </div> -->
            </div>
        </div>
        <div class="newsletter__image img-cover u-margin">
            <img src="<?php echo esc_url(get_field('s3_image')['url']); ?>" alt="<?php echo esc_attr(get_field('s3_image')['alt']); ?>" class="cover">
        </div>
    </div>
    <section class="s4-services-child m-300">
        <?php the_field('s4_text'); ?>
        <div class="button">
            <!-- <a href="<?php echo esc_url(get_permalink(get_page_by_title('About'))); ?>" class="main-button">VIEW OUR PROCESS</a> -->
        </div>
    </section>
    <section class="s5-services-child">
        <?php get_template_part('template', 'service'); ?>
    </section>
    <section class="s7-home discovery-form">
        <div class="line"></div>
        <img class="white-drift" src="<?php echo get_bloginfo('template_directory'); ?>/images/white-drift.svg" />
        <div class="contact-start">
            <h2 class="b50">Ready to get started on your project?</h2>
            <div class="z-index-compensation"><a class="main-button blue start-it">Begin project discovery form</a></div>
        </div>
        <div class="multi-step-contact">
            <?php echo do_shortcode('[contact-form-7 id="12127" title="Multi-Step Form"]'); ?>
        </div>
        <div class="message-in-route">
            <img src="<?php echo get_bloginfo('template_directory'); ?>/images/sent.svg" />
            <h2 class="b50 left">Your message is in route!</h2>
            <p>We will respond within 48 hours, but probably faster.</p>
        </div>
    </section>


    <?php if (get_field('projects')) : ?>
        <section class="s7-services-child">
            <h2 class="b40"><?php the_field('projects_header_text'); ?></h2>
            <div class="projects">
                <?php $post_objects = get_field('projects'); ?>
                <?php foreach ($post_objects as $post) :
                        setup_postdata($post); ?>
                    <div class="project">
                        <div class="img-cover">
                            <a href="<?php the_permalink(); ?>"></a>
                            <img src="<?php echo the_field('main_image'); ?>" alt="" class="cover">
                        </div>
                        <h2 class="b58"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <div class="button">
                <a href="/work-overview" class="main-button">check out more of our work</a>
            </div>
        </section>
    <?php endif; ?>
</main>


<?php get_footer(); ?>

<!-- 
    s2_side_text
    s2_text
    s3_image
    s4_text
    flexible_content
 -->