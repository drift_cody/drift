<?php
/*
Template Name: comp-test
*/
?>

<?php get_header(); ?>

<section class="m-200"></section>

<section class="comp-test u-pad">
    <div class="-row">
        <div class="header-hero header-hero__line">
            <img src="<?php echo esc_url(get_field('test_image')['url']) ?>" alt="" class="header-hero__image">
        </div>
    </div>
</section>


<section class="m-200"></section>

<?php get_footer(); ?>