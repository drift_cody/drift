<?php
/*
Template Name: Surprise Me
*/
?>

<?php get_header(); ?>

<main class="surprise-me">
    <section class="one-offs">
        <?php
        if (have_rows('images')) :
            while (have_rows('images')) : the_row(); ?>
                <div class="-row">
                    <div class="images-row">
                        <?php if (have_rows('image_row')) :
                                    while (have_rows('image_row')) : the_row(); ?>
                                <div class="images-row__image-container">
                                    <img src="<?php echo esc_url(get_sub_field('image')['url']); ?>" alt="" class="images-row__image">
                                </div>
                        <?php endwhile;
                                endif; ?>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>
    </section>


</main>



<?php get_footer(); ?>