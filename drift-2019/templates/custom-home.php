<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php
	// section 2
	$image = get_field('s2_video_thumbnail');
	$waldo_class = 's2-video-thumb-image';
	$waldo_styles = $waldo->waldoStylesArray($image, $waldo_class, $waldo_styles, $waldo_class);
?>

<main class="home">

	<div class="video-popup">
		<div class="close-it"></div>
		<div class="video-container">
			<div class="video">
				<div class="iframe-container">
					<div id="player"></div>
				</div>
			</div>
		</div>
	</div>
	
	<section class="s1-home contained">
		<div class="text">
			<h1 class="b58">
				<span class="first"><?php the_field('s1_big_text'); ?></span>
				<div class="second">
					<span><?php the_field('s1_big_text'); ?></span>
				</div>
			</h1>
		</div>
		<p class="sub-text">*The proof is in the pudding.</p>
		<div class="links">
			<?php $lbutton = get_field('s1_left_button'); $rbutton = get_field('s1_right_button'); ?>

			<?php if( $lbutton ): $link_url = $lbutton['url'];$link_title = $lbutton['title']; $link_target = $lbutton['target'] ? $lbutton['target'] : '_self'; ?>
				<a class="main-button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			<?php endif; ?>

			<?php if( $rbutton ): $link_url = $rbutton['url']; $link_title = $rbutton['title']; $link_target = $rbutton['target'] ? $rbutton['target'] : '_self'; ?>
				<a class="main-button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			<?php endif; ?>
		</div>
	</section>
	<section class="s2-home">
		<div class="s2-video-thumb">
			<div class="animation-container"><div class="s2-video-thumb-image"></div></div>
			<div class="line" data-anchor-target=".s2-video-thumb" data-center-top="height:calc(25% + 100px);" data-200-top="height:calc(50% + 137.5px);">
				<div class="play"></div>
			</div>
		</div>
	</section>
	<section class="s3-home">
		<img src="<?php echo get_bloginfo('template_directory');?>/images/checkmark.svg" class="checkmark" />
		<div class="text"><?php the_field('s3_big_text'); ?></div>
		<!-- <div class="logos">
			<?php if( have_rows('s3_featured_logos') ): while ( have_rows('s3_featured_logos') ) : the_row(); ?>
				<img src="<?php the_sub_field('logo'); ?>" />
			<?php endwhile; endif; ?>
		</div> -->
	</section>
	<section class="s4-home">
		<div class="line" data--100-bottom="height:75px;" data--500-bottom="height:345px;"></div>
		<h2 class="b40">Together – we create momentum</h2>
		<div class="case-studies">
			<?php $i = 1; if( have_rows('s4_case_studies') ): while ( have_rows('s4_case_studies') ) : the_row(); ?>
				<div id="case-study-<?php echo $i; ?>" class="case-study">
					<div class="info">
						<?php the_sub_field('text'); ?>
						<div class="link-container">
							<a href="<?php echo get_sub_field('link')['url']; ?>" class="dot-link"><?php echo get_sub_field('link')['title']?></a>
						</div>
					</div>
					
					<div class="image-container">
						<div class="animation-container">
							<a href="<?php echo get_sub_field('link')['url']; ?>">
								<img src="<?php the_sub_field('image_1'); ?>" class="spacer" />
								<!-- <div class="case-study-img" data-displacement="<?php echo get_bloginfo('template_directory');?>/images/displace.jpg" data-intensity="-0.8">
									<img src="<?php the_sub_field('image_1'); ?>" alt="Image" />
									<img src="<?php the_sub_field('image_2'); ?>" alt="Image Alt" />
								</div> -->
							</a>
						</div>
					</div>
					
				</div>
			<?php $i++; endwhile; endif; ?>
		</div>
		<div class="bottom">
			<div class="circle icon-heart"></div>
			<h2 class="b40">Like What You See?</h2>
			<a href="/work-overview" class="main-button">View our work</a>
		</div>
	</section>
	<section class="s5-home">
		<div class="top">
			<p class="sub-text">*MORE THAN A FEW TRICKS UP OUR SLEEVES</p>
			<h2 class="b58"><?php the_field('s5_big_text'); ?></h2>
		</div>
		<div class="services">
			<div class="service">
				<p class="big-number lapture">01</p>
				<?php echo get_field('s5_strategy',false); ?>
				<div class="link">
					<a href="<?php the_field('strategy_link'); ?>" class="dot-link">More About Branding</a>
				</div>
			</div>
			<div class="service">
				<p class="big-number lapture">02</p>
				<?php the_field('s5_branding'); ?>
				<div class="link">
					<a href="<?php the_field('branding_link'); ?>" class="dot-link">More About Applications</a>
				</div>
			</div>
			<div class="service">
				<p class="big-number lapture">03</p>
				<?php the_field('s5_web'); ?>
				<div class="link">
					<a href="<?php the_field('web_link'); ?>" class="dot-link">More About Web Design</a>
				</div>
			</div>
			<div class="service">
				<p class="big-number lapture">04</p>
				<?php the_field('s5_social'); ?>
				<div class="link">
					<a href="<?php the_field('social_link'); ?>" class="dot-link">More About Content Strategy</a>
				</div>
			</div>
		</div>
	</section>
	<section class="s6-home">
		<div class="line blog-line" data--50-bottom-top="height:50px;" data--500-bottom-top="height:275px;"></div>
		<div class="blog-posts">
			<?php 
			$args = array( 
				'post_type' => 'post',
				'posts_per_page' => '3'
			);
			$blog = new WP_Query( $args );
			if ( $blog->have_posts() ) : 
			?>
			<?php while( $blog->have_posts() ) : $blog->the_post() ?>
				<div class="post">
					<a href="<?php the_permalink(); ?>">
						<div class="image"><?php the_post_thumbnail(); ?></div>
						<?php the_title('<h2 class="b40">','</h2>'); ?>
					</a>
					<p class="date"><?php the_time('F j, Y'); ?></p>
				</div>
			<?php endwhile ?>
			<?php else : ?>
				<p>No posts to show.</p>
			<?php endif ?>
		</div>
		<div class="blog-posts mobile">
			<?php 
				$args = array( 
					'post_type' => 'post',
					'posts_per_page' => '3'
				);
				$blog = new WP_Query( $args );
				if ( $blog->have_posts() ) : 
				?>
				<?php while( $blog->have_posts() ) : $blog->the_post() ?>
					<div class="post">
						<a href="<?php the_permalink(); ?>">
							<div class="image"><?php the_post_thumbnail(); ?></div>
							<?php the_title('<h2 class="b40">','</h2>'); ?>
						</a>
						<p class="date"><?php the_time('F j, Y'); ?></p>
					</div>
				<?php endwhile ?>
				<?php else : ?>
					<p>No posts to show.</p>
			<?php endif ?>
		</div>
	</section>
	<section class="s7-home">
		<div class="line"></div>
		<img class="white-drift" src="<?php echo get_bloginfo('template_directory');?>/images/white-drift.svg" />
		<div class="contact-start">
			<h2 class="b50">Ready to get started on your project?</h2>
			<div class="z-index-compensation"><a class="main-button blue start-it">Begin project discovery form</a></div>
		</div>
		<div class="multi-step-contact">
			<?php echo do_shortcode('[contact-form-7 id="12127" title="Multi-Step Form"]'); ?>
		</div>
		<div class="message-in-route">
			<img src="<?php echo get_bloginfo('template_directory');?>/images/sent.svg" />
			<h2 class="b50 left">Your message is in route!</h2>
			<p>We will respond within 48 hours, but probably faster.</p>
		</div>
	</section>
</main>

<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>

<script>
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	// 3. This function creates an <iframe> (and YouTube player)
	//    after the API code downloads.
	var player;
	function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '600',
		width: '1000',
		videoId: 'z1AFTX1mMnc',
		playerVars:{ rel:0, loop:1},
		events: {
		'onReady': onPlayerReady,
		'onStateChange': onPlayerStateChange
		}
	});
	}

	// 4. The API will call this function when the video player is ready.
	function onPlayerReady() {
		$(".s2-video-thumb .play").click(function() {
			player.playVideo();
		});
	}

	// 5. The API calls this function when the player's state changes.
	//    The function indicates that when playing a video (state=1),
	//    the player should play for six seconds and then stop.
	var done = false;
	function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING && !done) {
		//setTimeout(stopVideo, 6000);
		done = true;
	}
	}
	function stopVideo() {
		player.stopVideo();
	}
	// $('header').addClass('fade-in');

	$(".s2-video-thumb .play").click(function() {
		$(".video-popup").addClass("active");
		$("body").addClass("no-scroll");
	});

	$(".close-it").click(function() {
		$(".video-popup.active").removeClass("active");
		$("body").removeClass("no-scroll");
		stopVideo();
	});

	$(document).ready(function(){
		// init controller
		if ($(window).width() < 992){
			$(".s1-home").addClass("active");
			$(".s2-video-thumb").addClass("active");
			$(".s3-home").addClass("active");
			$("#case-study-1").addClass("active");
			$("#case-study-2").addClass("active");
			$("#case-study-3").addClass("active");
			$("#case-study-4").addClass("active");
			$(".s5-home .b58 .animate-1").addClass("active");
			$(".s5-home .b58 .animate-2").addClass("active");
			$(".services").addClass("active");
			$(".blog-posts").addClass("active");
		} else {
			var controller = new ScrollMagic.Controller({globalSceneOptions: {}});
			// build scenes
			new ScrollMagic.Scene({triggerElement: ".s1-home"})
				.setClassToggle(".s1-home", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".s2-video-thumb"})
				.setClassToggle(".s2-video-thumb", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".s3-home"})
				.setClassToggle(".s3-home", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#case-study-1"})
				.setClassToggle("#case-study-1", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#case-study-2"})
				.setClassToggle("#case-study-2", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#case-study-3"})
				.setClassToggle("#case-study-3", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#case-study-4"})
				.setClassToggle("#case-study-4", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".s5-home"})
				.setClassToggle(".s5-home .b58 .animate-1", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".s5-home"})
				.setClassToggle(".s5-home .b58 .animate-2", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".s5-home"})
				.setClassToggle(".services", "active").reverse(false) // add class toggle
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: ".blog-posts"})
				.setClassToggle(".blog-posts", "active").reverse(false) // add class toggle
				.addTo(controller);
		}
	});

</script>

<?php get_footer(); ?>