<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>
<main class="services">
    <section class="s1-services">
        <p class="gotham">OUR SERVICES</p>
        <h1 class="b58"><?php the_field('header_text'); ?></h1>
        <div class="line"></div>
    </section>
    <section class="s2-services m-300">
        <div class="color-block"></div>
        <div class="img-cover">
            <img src="<?php echo esc_url(get_field('header_image')['url']); ?> " alt="<?php echo esc_attr(get_field('header_image')['alt']); ?> " class="cover">
        </div>
        <h2 class="b40"><?php the_field(s2_text); ?></h2>
        <p class="gotham"><?php the_field(s2_sub_text); ?></p>
        <div class="line-group">
            <div class="line"></div>
            <div class="circle icon-lightning-bolt"></div>
        </div>
    </section>

    <section class="s3-services m-300">
        <?php if (have_rows('services')) : ?>
            <?php while (have_rows('services')) : the_row(); ?>
                <div class="service-group">
                    <div class="left">
                        <div class="img-cover">
                            <img src="<?php echo esc_url(get_sub_field('image')['url']); ?>" alt="<?php echo esc_attr(get_sub_field('image')['alt']); ?>" class="cover">
                        </div>
                        <?php if (get_sub_field('project_link')) : ?>
                            <div class="link">
                                <a href="<?php the_sub_field('project_link'); ?>" class="dot-link">View Project</a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="right">
                        <?php the_sub_field('text'); ?>
                        <a href="<?php echo the_sub_field('services_individual_page'); ?>" class="main-button"><?php the_sub_field('button_text'); ?></a>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>
    </section>
    <section class="discovery-form">

    </section>
</main>

<?php get_footer(); ?>


<!-- header_image
s2_text
s2_sub_text
repeater 
    services
         image
        text
    services_individual_page
    project_link-->