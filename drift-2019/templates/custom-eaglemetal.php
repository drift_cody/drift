<?php
/*
Template Name: Case Study Eagle Metal
*/
?>

<?php get_header(); ?>
<main class="case-study eagle">
    <div class="video-popup">
        <div class="close-it"></div>
        <div class="vid-width">
            <div class="vid-con">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/k92-wfL5G00" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <section class="s1-eagle case-study-s1 m-400 s1-eagle-update">
        <!-- <p class="superscript">BURGER BAR & KITCHEN</p> -->
        <h1 class="b120">Eagle Metal</h1>
        <p class="gotham">Dallas, Texas</p>
    </section>
    <section class="case-study-s2 s2-eagle">
        <div class="img-cover">
            <div class="line" data-anchor-target=".img-cover" data-center-top="height:calc(25% + 45px);" data-200-top="height:calc(50% + 82.5px);">
                <div class="play"></div>
            </div>
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_thumbnail-min.jpg" alt="" class="cover">
        </div>
        <div class="bottom-content">
            <div class="story">
                <h2 class="b40">Eagle Metal manufactures connector plates for wood truss construction.</h2>
                <div class="story-content">
                    <h3 class="b20 gotham">THE STORY</h3>
                    <p>One of the biggest changes in their industry was in addition to manufacturing plates, competitors were differentiating themselves by also providing a truss design software platform. Truss manufacturers rely on eagle metal for the plates, and the software suite nearly runs their whole business.</p>
                </div>
            </div>
            <div class="what-we-did">
                <h3 class="b20 gotham">WHAT WE DID</h3>
                <ul class="ns">
                    <li>Positioning and Strategy</li>
                    <li>Brand Language</li>
                    <li>Visual Identity</li>
                    <li>Logo Design</li>
                    <li>Software Branding</li>
                    <li>Photoshoots</li>
                    <li>Brand Video</li>
                    <li>Product Videos</li>
                    <li>Copywriting</li>
                    <li>Marketing Campaigns</li>
                    <li>Print Collateral</li>
                    <li>Social Media Management</li>
                    <li>Tradeshow Booth Design</li>

                </ul>
            </div>
        </div>
    </section>
    <div class="case-study-content eagle">
        <div class="menu-anchor-top"></div>
        <div class="side-nav-container sticky" aria-hidden="true">
            <div class="side-nav bottom" aria-hidden="true">
                <ul>
                    <li class=""><a href="#branding"  ><p class="gotham">BRANDING</p></a></li>
                    <li class=""><a href="#website" ><p class="gotham">WEBSITE</p></a></li>
                    <li class=""><a href="#photography" ><p class="gotham">PHOTOGRAPHY</p></a></li>
<!--                     <li class=""><a href="#video" ><p class="gotham">VIDEO</p></a></li>
                    <li class=""><a href="#social" ><p class="gotham">SOCIAL</p></a></li>
                    <li class=""><a href="#results" ><p class="gotham">RESULTS</p></a></li> -->
                </ul>
            </div>
        </div>

        <section class="s3-eagle s3-eagle-update" id="branding">
            <div class="top">
                <h2 class="b173">Branding</h2>
            </div>
            <div class="center">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/logo.svg" alt="Eagle Metal Logo" />
                <h3><span>Empowering</span> great component manufacturers.</h3>
            </div>
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/branding-image.jpg" alt="" class="cover">
        </section>

        <section class="s4-eagle s4-eagle-update">
            <div class="white-box">
                <div class="top">
                    <p class="gotham">The Logo</p>
                    <h3>We started with a manufacturing company, and repositioned them to reflect the dual nature of their business. Part manufacturer. And part software developer.</h3>
                </div>
                <div class="bottom-left">
                    <p class="sub-text">Before</p>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/old-logo.svg" alt="Old Eagle Metal Logo" class="old-logo" />
                </div>
                <div class="bottom-right">
                    <p class="sub-text">After</p>
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/new-logo.svg" alt="New Eagle Metal Logo" class="new-logo" />
                </div>
            </div>
            <div class="bottom">
                <h3>Positioning Language</h3>
                <div class="language">
                    <div>
                        <p><strong>A Genuine Partnership</strong></p>
                        <p>Over 30 years in the industry means we understand. We adapt to the needs of every component manufacturer we work with, customizing and simplifying.</p>
                    </div>
                    <div>
                        <p><strong>Innovation that Empowers</strong></p>
                        <p>We believe in working smarter, not harder. Engineer solutions so our partners build more efficiently and reduce bottom lines. </p>
                    </div>
                    <div>
                        <p><strong>Bold Craftsmanship</strong></p>
                        <p>Our connector products are engineered, tested, and proven right here in the USA. For over 30 years, Eagle Metal has represented a commitment to strength & quality.</p>
                    </div>
                    <div>
                        <p><strong>TIME TO BUILD</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                    <div>
                        <p><strong>EMPOWERING GREAT COMPONENT MANUFACTURERS</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                    <div>
                        <p><strong>IT’S TIME FOR AN UPGRADE</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                </div>
                <div class="language-mobile">
                    <div>
                        <p><strong>A Genuine Partnership</strong></p>
                        <p>Over 30 years in the industry means we understand. We adapt to the needs of every component manufacturer we work with, customizing and simplifying.</p>
                    </div>
                    <div>
                        <p><strong>Innovation that Empowers</strong></p>
                        <p>We believe in working smarter, not harder. Engineer solutions so our partners build more efficiently and reduce bottom lines. </p>
                    </div>
                    <div>
                        <p><strong>Bold Craftsmanship</strong></p>
                        <p>Our connector products are engineered, tested, and proven right here in the USA. For over 30 years, Eagle Metal has represented a commitment to strength & quality.</p>
                    </div>
                    <div>
                        <p><strong>TIME TO BUILD</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                    <div>
                        <p><strong>EMPOWERING GREAT COMPONENT MANUFACTURERS</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                    <div>
                        <p><strong>IT’S TIME FOR AN UPGRADE</strong></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius tempor elit elit tortor sed ut eget phasellus varius tempor elit elit torto lore idfuer sed ut eget.</p>
                    </div>
                </div>
            </div>
            <h2 class="b173 bottom-text">#TimeToBuild</h2>
        </section>

        <section class="s5-eagle">
            <div class="side">
                <!-- <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/engineered-tested-true.svg" alt="Engineered. Tested. True." class="engineered" /> -->
                <p>It's Time for an Upgrade.</p>
            </div>
            <div class="content">
                <div class="left">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/truebuild-suite.svg" alt="TrueBuild Software Suite" class="truebuild-suite" />
                    <p>We designed a suite of icons for each of the products in the TrueBuild software suite, as well as custom loading screens, and various graphics to support the suite.</p>

                    <p>The truebuild brand has it’s own color pallete, to give it it’s own distinction, while still live under the EagleMetal umbrella.</p>
                </div>
                <div class="right">
                    <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/truebuild_software.svg" alt="TrueBuild Software" class="software" />
                </div>
            </div>
            <!-- <div class="brochure"></div> -->
        </section>

        <section class="s6-eagle">
            <div class="top">
                <h2 class="b58">We focused the rebrand on building up the target customer, and making them – the truss manufacturer – the hero of the Eagle Metal story.</h2>
            </div>
        </section>
        <section class="s7-eagle">
            <div class="stuff">
                <div class="splash-slider">
                    <div>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img1-min.jpg" alt="Eagle Metal Website" />
                    </div>
                </div>
            </div>
        </section>
        <section class="s8-eagle" id="website">
            <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/website_img-min.jpg" alt="" class="cover">
            <div class="top">
                <h2 class="b173">Website</h2>
            </div>
            <div class="right">
                <h3 class="b40">The website is a showcase for the powerful software tools, and the many ways it can save their customer’s time and expenses.</h3>
                <a href="https://eaglemetal.com" target="_blank" class="main-button">View the website</a>
            </div>
        </section>
        <section class="s9-eagle">
            <div class="top">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_homepage_long-min.jpg" alt="Eagle Metal Website Homepage"/>
            </div>
            <div class="center">
                <h3 class="b58">The website is a showcase for the powerful software tools, and the many ways it can save their customer’s time and expenses.</h3>
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_icon_set.svg" alt="" class="icons">
            </div>
            <div class="bottom">
                <div class="image-row">
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img2-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                </div>
                <div class="image-row">
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img3-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img4-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                </div>
                <div class="image-row">
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img5-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img6-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img7-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img8-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                </div>
                <div class="image-row">
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img9-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img10-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                </div>
                <div class="image-row">
                    <div class="image-container">
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/eagle_workpage_img11-min.jpg" alt="Eagle Metal Website Interior Pages"/>
                    </div>
                </div>
            </div>
        </section>
        <section class="s10-eagle" id="photography">
            <div class="top">
                <h2 class="b173">Video & <br/>Photography</h2>
            </div>
            <div class="bottom">
                <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal-update/video_photography_img-min.jpg" alt="" class="cover"/>
            </div>
        </section>
        <section class="s11-eagle">
            <div class="left">
                <h3 class="b40">Creative Direction</h3>
                <p>We use a variety of photographic approaches to showcase the software in different offices, by different users, etc.</p><br/>

                <p>Our photography and video has captured the process of truss fabrication, from the initial manufacturing of the plates, to delivery, and assembling, and finally installation.</p>
            </div>
            <div class="right">
                <p class="sub-text">What we did</p>
                <div class="services">
                    <div>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/camera.svg" alt="" class="camera"/>
                        <p>Corporate Photography</p>
                    </div>
                    <div>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/camera-drone.svg" alt="" class="camera-drone"/>
                        <p>Drone Photography</p>
                    </div>
                    <div>
                        <img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/portrait.svg" alt="" class="portrait"/>
                        <p>Staff Photography</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="s12-eagle">
            <div class="image image-1"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/construction.jpg" alt="Eagle Metal Construction Photo" /></div>
            <div class="image image-2"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/computer.jpg" alt="Eagle Metal Software Use Photo" /></div>
            <div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>
            <div class="image image-3"><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/eagle-photo-1.jpg" alt="Eagle Metal Truss" /></div>

            <div class="photography-mobile-slider">
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/computer.jpg" alt="Eagle Metal Software Use Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/houses.jpg" alt="Eagle Metal Houses Photo" /></div>
                <div><img src="<?php bloginfo('template_url');?>/images/case-studies/eagle-metal/eagle-photo-1.jpg" alt="Eagle Metal Truss" /></div>
            </div>

            <h3 class="b40 mobile-statement">Statement about photography. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed </h3>

        </section>
        <section class="s13-eagle">
            <div class="line"></div>
            <div class="text">
                <p class="sub-text">More work this way</p>
                <a href="/grub" class="dot-link">Grub</a>
            </div>
        </section>

        <div class="menu-anchor-end"></div>
    </div>
</main>
<script>
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".menu-anchor-end").offset().top;
        var div_top = $('.menu-anchor-top').offset().top;
        var div_height = $(".sticky").height();
        var padding = 0;  // tweak here or get from margins etc
        
        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({top: (window_top + div_height - footer_top + padding) * -1})
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.sticky').css({top:0})
        } else {
            $('.sticky').removeClass('stick');
        }
    }
    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
        
    });
    $( document ).ready(function() {
        $('.splash-slider').slick({
            slidesToShow: 1
        });  
        $('.photography-mobile-slider').slick({
            slidesToShow: 1
        }); 
        $('.language-mobile').slick({
            slidesToShow: 1
        }); 
    });

    $(".play").click(function() {
        $(".video-popup").addClass("active");
        $("body").addClass("no-scroll");
    });

    $(".close-it").click(function() {
        $(".video-popup.active").removeClass("active");
        $("body").removeClass("no-scroll");
    });
</script>
<?php get_footer(); ?>