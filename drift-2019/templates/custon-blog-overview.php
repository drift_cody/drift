<?php
/*
Template Name: Blog Overview
*/
?>

<?php get_header(); ?>

<main class="blog-overview">
    <section class="s1-blog-overview">
        <h1 class="b58"><?php the_field('heading'); ?></h1>
        <p class="gotham">THE DRIFT BLOG</p>
        <div class="red-line"></div>
    </section>
    <!-- <section class="s2-blog-overview m-50">
        <nav>
            <h4>Categories:</h4>
            <?php echo facetwp_display( 'facet', 'categories' ); ?>
            <h5 class="gotham">ALL</h5>
            <h5 class="gotham">BRANDING</h5>
            <h5 class="gotham">WEBSITES</h5>
            <h5 class="gotham">TIPS</h5>
            <h5 class="gotham">CAREERS</h5>
            <h5 class="gotham">TEAM</h5>
        </nav>
    </section> -->
    <section class="s3-blog-overview m-130">
        <?php echo facetwp_display( 'template', 'blog' ); ?>
        <a class="main-button fwp-load-more">Load More</a>
    </section>
</main>

<?php get_footer(); ?>