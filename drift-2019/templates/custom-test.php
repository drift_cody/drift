<?php
/*
Template Name: test
*/
get_header();

$twoImages=get_field('two_images');
$threeImages=get_field('three_images');
$oneImage=get_field('one_image');

?>

<div class="test-section">

</div>

<div class="row-images">
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($twoImages['image1']);  ?>" alt="" class="row-images__image">
    </div>
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($twoImages['image2']);  ?>" alt="" class="row-images__image">
    </div>
</div>

<div class="row-images">
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($oneImage['image1']);  ?>" alt="" class="row-images__image">
    </div>
</div>
<div class="row-images">
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($threeImages['image1']);  ?>" alt="" class="row-images__image">
    </div>
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($threeImages['image2']);  ?>" alt="" class="row-images__image">
    </div>
    <div class="row-images__image-container">
        <img src="<?php echo esc_url($threeImages['image3']);  ?>" alt="" class="row-images__image">
        <div class="row-images__play-button"></div>
    </div>
</div>

<div class="row-images">
        <div class="row-images__slideshow-container">
            <div class="row-images__slideshow-buttons">
                <div id="slideshow-1-prev" class="button__slideshow"></div>
                <div id="slideshow-1-next" class="button__slideshow"></div>
            </div>
            <div class="row-images__slideshow">
                <img src="<?php echo esc_url($oneImage['image1']);  ?>" alt="" class="row-images__image">
                <img src="<?php echo esc_url($oneImage['image1']);  ?>" alt="" class="row-images__image">
                <img src="<?php echo esc_url($oneImage['image1']);  ?>" alt="" class="row-images__image">
                <img src="<?php echo esc_url($oneImage['image1']);  ?>" alt="" class="row-images__image">
            </div>  
        </div>
        
</div>



<div class="test-section">
    
</div>
<script>
    $(document).ready(function(){
        $('.row-images__slideshow').slick({
            lazyLoad: "progressive",
            dots: false,
            prevArrow: $('#slideshow-1-prev'),
            nextArrow: $('#slideshow-1-next'),
        });
    });
</script>
<?php get_footer(); ?>