<?php
/*
Template Name: Case Study Mid South
*/
?>

<?php get_header(); ?>
<main class="case-study midSouth">
    <section class="s1-midSouth case-study-s1 m-400">
        <h1 class="b120">MidSouth Electric Co-Op</h1>
    </section>
    <section class="case-study-s2 s2-midSouth">
        <div class="img-cover">
            <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/hero-image.jpg" alt="" class="cover">
        </div>
        <div class="bottom-content">
            <div class="story">
                <h2 class="b40">MidSouth is a forward-thinking energy cooperative in Texas.</h2>
                <div class="story-content">
                    <h3 class="b20 gotham">THE STORY</h3>
                    <p>With 80 years of history, our approach has always been to honor the MidSouth past, while bringing a fresh, bold approach to all forms of digital and print communication. We’ve helped bridge the gap between cooperative and local communities by telling impactful stories.</p>
                    <div class="button-container">
                        <a href="https://midsouthsynergy.com/" target="_blank" class="main-button">Visit the website</a>
                    </div>
                </div>
            </div>
            <h3 class="b20 gotham">WHAT WE DID</h3>
            <ul class="ns">
                <li>Corporate Rebrand</li>
                <li>Visual Brand Systems</li>
                <li>Logo Design</li>
                <li>Messaging</li>
                <li>Print Design</li>
                <li>Social Media Consulting</li>
                <li>Video Campaigns</li>
                <li>Digital Marketing</li>
                <li>Copywriting</li>
                <li>Custom Wordpress Website</li>
            </ul>

        </div>
    </section>
    <div class="case-study-content">
        <div class="menu-anchor-top"></div>
        <div class="side-nav-container sticky" aria-hidden="true">
            <div class="side-nav bottom" aria-hidden="true">
                <ul>
                    <li class=""><a href="#branding">
                            <p class="gotham">BRANDING</p>
                        </a></li>
                    <li class=""><a href="#website">
                            <p class="gotham">WEBSITE</p>
                        </a></li>
                    <li class=""><a href="#responsive">
                            <p class="gotham">RESPONSIVE</p>
                        </a></li>
                </ul>
            </div>
        </div>
        <section class="case-study-s3 s3-midSouth" id="branding">

            <div class="top">
                <h2 class="b173">Branding</h2>
                <div class="img-container">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/color-chart.png" alt="" class="">
                </div>
            </div>
            <div class="bottom">
                <div class="bottom-top">
                    <div class="-left">
                        <p class="b40 lapture">The new identity is vibrant, fresh and modern while still paying homage to the original, very well recognized logo.</p>
                    </div>
                    <div class="-right"></div>
                </div>
                <div class="bottom-left">
                    <p class="tag">BEFORE</p>
                    <div class="img-container">

                        <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/before-logo.png" alt="">

                    </div>
                </div>
                <div class="bottom-right">
                    <p class="tag">AFTER</p>
                    <div class="img-container">

                        <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/after-logo.png" alt="">

                    </div>
                </div>
            </div>
        </section>
        <section class="case-study-s4 s4-midSouth">
            <div class="left">
                <h2 class="lapture b40">Beyond an Electric Co-Op</h2>
                <p class="gotham b18">The MidSouth brand has multiple sub-brands, a reflection of their progressive efforts, and within each sub-brand we created color variations, as well as patterns that reflect each of the branches. The icon has three subtle changes in color, which reflects the multi-layered services offered, and can change to reflect each sub-brand palette. </p>
            </div>
            <div class="right">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/logos.png" alt="" class="cover">
                </div>
            </div>
        </section>
        <section class="case-study-s5 s5-midSouth">
            <div class="segment internet">

                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/internet.png" alt="" class="cover">
                </div>

            </div>
            <div class="segment water">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/midsouth-water.png" alt="" class="cover">
                </div>
            </div>
            <div class="segment solar">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/internet.png" alt="" class="cover">
                </div>
            </div>
            <div class="segment vehicle">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/midsouth-vehicle.png" alt="" class="cover">
                </div>
            </div>
        </section>
        <section class="case-study-s6 s6-midSouth">
            <div class="image-row">
                <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/midsouth-truck.jpg" alt="" class="">
            </div>
            <div class="image-row">
                <div class="left">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/employee.jpg" alt="" class="">
                </div>
                <div class="right">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/magazine.jpg" alt="" class="">
                </div>
            </div>
            <div class="color-block"></div>
        </section>
        <section class="case-study-s7 s7-midSouth" id="website">
            <div class="color-block"></div>
            <div class="top">
                <h2 class="b173">Website</h2>
                <p class="lapture b40">Finding the right information, at the right time, was critical for MidSouth customers.</p>
            </div>
            <div class="bottom">
                <div class="img-cover">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/website-layout.png" alt="" class="cover">
                </div>
            </div>
        </section>
        <section class="case-study-s8 s8-midSouth">
            <h2 class="b58 lapture">The new website made information easier to find. The result? Less calls to the office. More happy customers.</h2>
        </section>
        <section class="case-study-s9 s9-midSouth" id="responsive">
            <h2 class="b173">Responsive</h2>
            <div class="image-row">
                <div class="img-container">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/midsouth-ipads.jpg" alt="" class="">
                </div>
            </div>
            <div class="color-block"></div>
        </section>
        <section class="case-study-s10 s10-midSouth">
            <div class="image-row">
                <div class="left">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/mobile.jpg" alt="" class="">
                </div>
                <div class="right">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/mobile2.jpg" alt="" class="">
                </div>
            </div>
        </section>
        <section class="case-study-s11 s11-midSouth">
            <h2 class="b173">Branding</h2>
            <div class="img-cover">
                <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/branding1.jpg" alt="" class="cover">
            </div>
        </section>
        <div class="menu-anchor-end"></div>
    </div>
    <section class="case-study-s12 s12-midSouth">
        <div class="top">
            <div class="left">
                <div class="image-container">
                    <img src="<?php bloginfo('template_url'); ?>/images/case-studies/midSouth/yourcrew.png" alt="">
                </div>
            </div>
            <div class="right">
                <p class="gotham b18">Storms take out power lines. And when the power is down, customers are unhappy. We created a social video campaign to connect customers to the linemen. The campaign videos helped create opportunities for empathy, and a more personalized understanding. Results? Less negative reviews, more positive social feedback.</p>
            </div>
        </div>
        <div class="bottom">
            <!-- <div class="playline active">
                <div class="line"></div>
                <div class="circle"><p>play</p></div>
            </div> -->
            <div class="main-slider">
                <div class="item youtube fluid-video">
                    <iframe class="embed-player slide-media" width="560" height="315" src="https://www.youtube.com/embed/aG-5HeijqDo?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&controls=0&loop=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="item youtube fluid-video">
                    <iframe class="embed-player slide-media" width="560" height="315" src="https://www.youtube.com/embed/MeMbA1NbrR0?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&controls=0&loop=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="item youtube fluid-video">
                    <iframe class="embed-player slide-media" width="560" height="315" src="https://www.youtube.com/embed/rmahMvr4QUU?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&controls=0&loop=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="navigation">
                <div class="slider-prev icon-arrow-nc"></div>
                <div class="slider-next icon-arrow-nc"></div>
            </div>
        </div>
        <div class="color-block"></div>
    </section>
    <section class="s13-midSouth last">
        <div class="line"></div>
        <p class="gotham">ON TO THE NEXT ONE</p>
        <div class="next">
            <a href="/that-conference" class="dot-link next">That Conference</a>
            <!-- <a href="<?php bloginfo('template_url'); ?>/that-conference" class="dot-link next">That Conference</a> -->
        </div>
    </section>

</main>
<script>
    var slideWrapper = $(".main-slider"),
        iframes = slideWrapper.find('.embed-player'),
        lazyImages = slideWrapper.find('.slide-image'),
        lazyCounter = 0,
        playLine = $(".playline");

    // POST commands to YouTube or Vimeo API
    function postMessageToPlayer(player, command) {
        if (player == null || command == null) return;
        player.contentWindow.postMessage(JSON.stringify(command), "*");
    }

    // When the slide is changing
    function playPauseVideo(slick, control) {
        var currentSlide, slideType, startTime, player, video;
        currentSlide = slick.find(".slick-current");
        slideType = currentSlide.attr("class").split(" ")[1];
        player = currentSlide.find("iframe").get(0);
        startTime = currentSlide.data("video-start");

        switch (control) {
            case "play":
                postMessageToPlayer(player, {
                    "event": "command",
                    "func": "playVideo"
                });
                break;
            case "pause":
                postMessageToPlayer(player, {
                    "event": "command",
                    "func": "pauseVideo"
                });
                break;
        }

    }

    // Resize player
    function resizePlayer(iframes, ratio) {
        if (!iframes[0]) return;
        var win = $(".main-slider"),
            width = win.width(),
            playerWidth,
            height = win.height(),
            playerHeight,
            ratio = ratio || 16 / 9;

        iframes.each(function() {
            var current = $(this);
            if (width / ratio < height) {
                playerWidth = Math.ceil(height * ratio);
                current.width(playerWidth).height(height).css({
                    left: (width - playerWidth) / 2,
                    top: 0
                });
            } else {
                playerHeight = Math.ceil(width / ratio);
                current.width(width).height(playerHeight).css({
                    left: 0,
                    top: (height - playerHeight) / 2
                });
            }
        });
    }

    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".menu-anchor-end").offset().top;
        var div_top = $('.menu-anchor-top').offset().top;
        var div_height = $(".sticky").height();
        var padding = 0; // tweak here or get from margins etc

        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({
                top: (window_top + div_height - footer_top + padding) * -1
            })
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.stick').css({
                top: 0
            })
        } else {
            $('.sticky').removeClass('stick');
        }
    }

    // DOM Ready
    $(function() {
        // Initialize
        slideWrapper.on("init", function(slick) {
            slick = $(slick.currentTarget);
            //setTimeout(function(){
            playPauseVideo(slick, "play");

            //}, 1000);
            //resizePlayer(iframes, 16/9);
        });
        slideWrapper.on("beforeChange", function(event, slick) {
            slick = $(slick.$slider);
            playPauseVideo(slick, "pause");
        });
        slideWrapper.on("afterChange", function(event, slick) {
            slick = $(slick.$slider);
            playPauseVideo(slick, "play");
        });
        slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
            lazyCounter++;
            if (lazyCounter === lazyImages.length) {
                lazyImages.addClass('show');
                slideWrapper.slick("slickPlay");
            }
        });

        //start the slider
        slideWrapper.slick({
            fade: true,
            autoplaySpeed: 4000,
            lazyLoad: "progressive",
            speed: 600,
            dots: false,
            cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)",
            prevArrow: $('.slider-prev'),
            nextArrow: $('.slider-next'),
            adaptiveHeight: true
        });
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });



    // Resize event
    // $(window).on("resize.slickVideoPlayer", function(){  
    // resizePlayer(iframes, 16/9);
    // });
</script>
<?php get_footer(); ?>