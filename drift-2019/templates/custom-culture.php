<?php
/*
Template Name: Culture
*/
?>

<?php get_header(); ?>

<main class="culture">
    <section class="s1-culture m-230" identifier="s1">
        <?php the_field('header_text');?>
        <p class="gotham">READY TO MEET THE DRIFT TEAM?</p>
        <div class="line"></div>
    </section>
    <div class="main-content">
        <div class="sticky-anchor2"></div>
        <div class="side-nav-container sticky2" aria-hidden="true">
            <!-- <div class="side-nav overlay" >
                <ul>
                    <li class=""><a href="/origin-story"  ><p class="gotham">ORIGIN STORY</p></a></li>
                    <li class=""><a href="#team-section" ><p class="gotham">THE TEAM</p></a></li>
                    <li class=""><a href="#core-values" ><p class="gotham">CORE VALUES</p></a></li>
                </ul>
            </div> -->
            <div class="side-nav bottom" aria-hidden="true">
                <ul>
                    <!-- <li class=""><a href="#origin-story"  ><p class="gotham">ORIGIN STORY</p></a></li> -->
                    <li class=""><a href="#team-section" ><p class="gotham">THE TEAM</p></a></li>
                    <li class=""><a href="#core-values" ><p class="gotham">CORE VALUES</p></a></li>
                </ul>
            </div>
            <!-- <div class="alpha-container">
                <div class="side-nav last" aria-hidden="true">
                    <ul>
                        <li class=""><a href="/origin-story"  ><p class="gotham">ORIGIN STORY</p></a></li>
                        <li class=""><a href="#team-section" ><p class="gotham">THE TEAM</p></a></li>
                        <li class=""><a href="#core-values" ><p class="gotham">CORE VALUES</p></a></li>
                    </ul>
                </div>
            </div> -->
        </div>
        <section class="s2-culture m-230" id="origin-story" identifier="s2">
            <div class="color-block"></div>
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('group_image')['url']); ?>" alt="<?php echo esc_attr(get_field('group_image')['alt']); ?>" class="cover">
            </div>
            <?php the_field('s2_text'); ?>
            <!-- <div class="button">
                <a href="/origin-story" class="dot-link">READ OUR ORGIN STORY</a>
            </div> -->
            <div class="line-group">
                <div class="line"></div>
                <div class="circle icon-lightning-bolt"></div>
            </div>
        </section>
        <section class="s3-culture" id="team-section" identifier="s3">
            <?php $args = array('post_type' => 'drifter', 'order' => 'ASC'); ?>
            <?php $loop = new WP_Query($args); ?>
            <?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <a href="<?php the_permalink()?>" class="drifter">
                        <div class="img-cover">
                            <img src="<?php echo esc_url(get_field('profile_image')['url']); ?>" alt="<?php echo esc_attr(get_field('profile_image')['alt']); ?>" class="cover">
                            <img src="<?php echo esc_url(get_field('overlay_image')['url']); ?>" alt="<?php echo esc_attr(get_field('overlay_image')['alt']); ?>" alt="" class="cover over">
                        </div>
                        <h2><?php the_title(); ?></h2>
                        <p class="gotham"><?php the_field('title'); ?></p>
                    </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </section>
        <section class="s4-culture image-section-3 m-230" identifier="s4">
            <?php $topImages = get_field('three_photos_top'); ?>
            <div class="image-row">
                <div class="image">
                    
                    <img src="<?php echo esc_url( $topImages['image1']['url']);?>" alt="<?php echo esc_attr( $topImages['image1']['alt']); ?>" class="work-image">
                </div>
                <div class="image">
                    <img src="<?php echo esc_url( $topImages['image2']['url']); ?>" alt=" <?php echo esc_attr( $topImages['image2']['alt']); ?>" class="work-image">
                </div>
            </div>
            <div class="image-row">
                <div class="image">
                    <img src="<?php echo esc_url( $topImages['image3']['url']); ?>" alt="<?php echo esc_attr( $topImages['image3']['alt']); ?>" class="work-image">
                </div>
            </div>
        </section>
        <section class="s5-culture" id="core-values" identifier="s5">
            <?php if( have_rows('scrolling_text') ): while(have_rows('scrolling_text')) : the_row();?>
                        <div class="scrolling-text" style="background:<?php echo the_sub_field('background_color') ?>;">
                            <div class="sticky-anchor"></div>
                            <div class="text sticky">
                                <?php echo the_sub_field('left_text'); ?>
                                <!-- <a href="#" class="main-button nc"><p><?php echo the_sub_field('button_text'); ?></p></a> -->
                            </div>
                            <div class="image">
                                <?php $i = 01; if(have_rows('right_text')) : while(have_rows('right_text')) : the_row();  ?>
                                    <div class="item">   
                                        <h2><span>0<?php echo $i ?></span><?php echo the_sub_field('heading') ?></h2>
                                        <?php echo the_sub_field('text') ?>
                                    </div>
                                <?php $i++; endwhile; endif; ?>
                            </div>
                        </div>
                        <div class="scrolling-text-end"></div>
            <?php endwhile; endif; ?>
        </section>
        <section class="s6-culture image-section-3 m-230" identifier="s6">
            <?php $topImages = get_field('three_photos_bottom'); ?>
            <div class="image-row">
                <div class="image">
                        <img src="<?php echo $topImages['image1']['url']; ?>" alt="<?php echo esc_attr( $topImages['image1']['alt']); ?>" class="work-image">
                </div>
                <div class="image">
                    <div >
                        <img src="<?php echo $topImages['image2']['url']; ?>" alt="<?php echo esc_attr( $topImages['image2']['alt']); ?>" >
                    </div>
                </div>
            </div>
            <div class="image-row">
                <div class="image">
                    <img src="<?php echo $topImages['image3']['url']; ?>" alt="<?php echo esc_attr( $topImages['image3']['alt']); ?>" class="work-image">
                </div>
            </div>
        </section>
        <div class="scrolling-text-end2"></div>
    </div>
</main>
<script>
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".scrolling-text-end").offset().top;
        var div_top = $('.sticky-anchor').offset().top;
        var div_height = $(".sticky").height();
        
        var padding = 180;  // tweak here or get from margins etc
        
        if (window_top + div_height > footer_top - padding)
            $('.sticky').css({top: (window_top + div_height - footer_top + padding) * -1})
        else if (window_top > div_top) {
            $('.sticky').addClass('stick');
            $('.sticky').css({top: 0})
        } else {
            $('.sticky').removeClass('stick');
        }
    }
    function sticky_relocate2() {
        var window_top = $(window).scrollTop();
        var footer_top = $(".scrolling-text-end2").offset().top;
        var div_top = $('.sticky-anchor2').offset().top;
        var div_height = $(".sticky2").height();
        
        var padding = 200;  // tweak here or get from margins etc
        
        if (window_top + div_height > footer_top - padding)
            $('.sticky2').css({top: (window_top + div_height - footer_top + padding) * -1})
        else if (window_top > div_top) {
            $('.sticky2').addClass('stick');
            $('.sticky2').css({top:300})
        } else {
            $('.sticky2').removeClass('stick');
        }
    }
    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
        $(window).scroll(sticky_relocate2);
        sticky_relocate2();
        
    });

</script>

<?php get_footer(); ?>