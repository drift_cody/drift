<?php
/*
Template Name: Work Overview
*/
?>

<?php get_header(); ?>

<main class="work-overview">
	<section class="s1-work-overview">
		<div class="text contained">
			<?php the_field('s1_big_text'); ?>
			<div class="line"></div>
			<h2 class="b58">Our Case Studies</h2>
			<!-- <p class="sub-text">Have you seen our <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Case Studies' ) ) ); ?>">Case Studies</a>?</p> -->
		</div>
	</section>
	<section class="s2-work-overview">
		<div class="case-studies">

        <?php
        if (have_rows('case_studies')) :
            while (have_rows('case_studies')) : the_row(); ?>
                <div class="case-study">
                    <div class="img-cover">
                        <a href="<?php the_sub_field('page'); ?>"></a>
                        <img src="<?php the_sub_field('featured_image'); ?>" alt="" class="cover">
                    </div>

                    <h2 class="b58"><a href="<?php the_sub_field('page'); ?>"><?php the_sub_field('title'); ?></a></h2>
                    <?php if (get_sub_field('sub_text')) : $tags = get_sub_field('sub_text'); ?>
                        <ul class="ns">
                            <?php foreach ($tags as $tag) : ?>
                                <li style="text-transform: uppercase"><?php echo $tag; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
        <?php endwhile;
        endif; ?>

    </div>
	</section>
	<section class="s3-work-overview">
		<div class="text">
			<div class="line"></div>
			<h2 class="b58">More of our Work</h2>
		</div>
		<?php if( have_rows('s2_work_pages') ): while ( have_rows('s2_work_pages') ) : the_row(); ?>
			<div class="work">
				<?php if(get_sub_field('row_layout') == 1) { ?>
					<!-- big guy -->
					<div class="full">
						<?php if( have_rows('project_1') ): while ( have_rows('project_1') ) : the_row(); ?>
							<?php $projectName = get_the_title(get_sub_field('project')); ?>
							<?php $projectLink = get_permalink(get_sub_field('project')); ?>
							<?php $projectTags = get_the_tags(get_sub_field('project')); ?>
							<a href="<?php echo $projectLink; ?>"><div class="image-container">
								<img class="cover" src="<?php the_sub_field('image'); ?>" />
							</div></a>
							<a href="<?php echo $projectLink; ?>"><h2 class="b58"><?php echo $projectName; ?></h2></a>
							<ul class="tags">
								<?php if ($projectTags) { foreach($projectTags as $tag) { ?>
								    <li><?php echo $tag->name; ?></li>
								<?php } } ?>
							</ul>
						<?php endwhile; endif; ?>
					</div>
				<?php } else { ?>
					<!-- lil guys -->
					<div class="half">
						<?php if( have_rows('project_1') ): while ( have_rows('project_1') ) : the_row(); ?>
							<?php $projectName = get_the_title(get_sub_field('project')); ?>
							<?php $projectLink = get_permalink(get_sub_field('project')); ?>
							<?php $projectTags = get_the_tags(get_sub_field('project')); ?>
							<a href="<?php echo $projectLink; ?>"><div class="image-container">
								<img class="cover" src="<?php the_sub_field('image'); ?>" />
							</div></a>
							<a href="<?php echo $projectLink; ?>"><h2 class="b58"><?php echo $projectName; ?></h2></a>
							<ul class="tags">
								<?php if ($projectTags) { foreach($projectTags as $tag) { ?>
								    <li><?php echo $tag->name; ?></li>
								<?php } } ?>
							</ul>
						<?php endwhile; endif; ?>
					</div>
					<div class="half">
						<?php if( have_rows('project_2') ): while ( have_rows('project_2') ) : the_row(); ?>
							<?php $projectName = get_the_title(get_sub_field('project')); ?>
							<?php $projectLink = get_permalink(get_sub_field('project')); ?>
							<?php $projectTags = get_the_tags(get_sub_field('project')); ?>
							<a href="<?php echo $projectLink; ?>"><div class="image-container">
								<img class="cover" src="<?php the_sub_field('image'); ?>" />
							</div></a>
							<a href="<?php echo $projectLink; ?>"><h2 class="b58"><?php echo $projectName; ?></h2></a>
							<ul class="tags">
								<?php if ($projectTags) { foreach($projectTags as $tag) { ?>
								    <li><?php echo $tag->name; ?></li>
								<?php } } ?>
							</ul>
						<?php endwhile; endif; ?>
					</div>
				<?php } ?>
			</div>
		<?php endwhile; endif; ?>
	</section>
</main>

<?php get_footer(); ?>