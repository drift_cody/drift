<?php
/*
Template Name: Drift Academy
*/
?>

<?php get_header('academy'); ?>

<main class="academy-landing">
	<section class="s1-academy-landing">
		<p class="superscript"><?php the_field('s1_top_text'); ?></p>
		<div class="left">
			<?php the_field('s1_left_text'); ?>
		</div>
		<div class="right">
			<?php the_field('s1_right_text'); ?>
		</div>
	</section>
	<section class="s2-academy-landing">
		<div class="form">
			<!-- Begin Mailchimp Signup Form -->
			<div id="mc_embed_signup">
				<form action="https://driftingcreatives.us12.list-manage.com/subscribe/post?u=cbe005d9830b5e01dbd327281&amp;id=a0fe0eadea" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <div id="mc_embed_signup_scroll">
						<div class="mc-field-group">
							<label for="mce-EMAIL">Email Address </label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						</div>
						<div class="mc-field-group">
							<label for="mce-FNAME">First Name </label>
							<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
						</div>
						<div class="mc-field-group">
							<label for="mce-LNAME">Last Name </label>
							<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
						</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cbe005d9830b5e01dbd327281_a0fe0eadea" tabindex="-1" value=""></div>
					    <div class="mc-field-group submission">
					    	<input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button">
					    </div>
				    </div>
				</form>
			</div>
			<!--End mc_embed_signup-->
		</div>
		<div class="text">
			<?php the_field('s2_text'); ?>
		</div>
	</section>
</main>

<?php get_footer('academy'); ?>