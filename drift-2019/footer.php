<footer>
	<div class="top-footer">
		<div class="left">
			<div class="left-l">
				<?php if(get_field('job_openings','option')): ?>
				<h4>Interested in joining our team?</h4>
				<a class="main-button" href="/careers">Job openings</a> 
				<?php endif; ?>
			</div>
			<div class="left-r">
				<h4>We work with customers across the nation.</h4>
				<div class="offices">
					<div class="office">
						<p class="title">COLLEGE STATION</p>
						<p>909 Harvey Rd Suite D, <br/>College Station, TX 77840</p>
					</div>
					<div class="office">
						<p class="title">DALLAS</p>
						<p>5805 Wilford Dr<br/>McKinney, TX 75070</p>
					</div>
				</div>
			</div>
		</div>
		<div class="right">
			<a href="/surprise-me"><div class="surprise">Surprise Me</div></a>
		</div>
	</div>
	<div class="bottom-footer">
		<img class="lightning" src="<?php echo get_bloginfo('template_directory');?>/images/lightning.svg" />
		<p>DRIFTING CREATIVES, LLC. COPYRIGHT 2019 AND FOREVER. <a href="#">LET’S HAVE SOME FUN.</a></p>
	</div>
</footer>

		<?php wp_footer(); ?>

		<script>
			if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
			    skrollr.init({
			        forceHeight: false
			    });
			}
			if($(window).width() < 980){
				skrollr.init().destroy();
			}
		</script>

		<script>
			imagesLoaded( document.querySelectorAll('img'), () => {
				document.body.classList.remove('loading');
			});

			Array.from(document.querySelectorAll('.case-study-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});


		</script>

	    <!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/409445/animateAnything.js"></script>
	
	</body>
</html>