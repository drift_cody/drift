<?php get_header(); ?>

<main class="drifter-page">

	<section class="s1-drifter ">
		<h1 class="b58"><?php the_title() ?></h1>
		<p class="sub-text"><?php the_field('title'); ?></p>
	</section>
    <section class="s2-drifter m-200">
        <h2 class="b40">"<?php the_field('quote') ?>"</h2>
        <p class="p-25">-<?php the_field('byline');?></p>
        <div class="red-line">
            <div class="line"></div>
            <div class="circle <?php chooseIcon(get_the_title()) ?>"></div>
        </div>
    </section>
    <section class="s3-drifter m-230">
        <div class="img-cover">
            <img src="<?php the_field('hero_image')['url'];?>" alt="" class="cover">
        </div>
    </section>
    <section class="s4-drifter">
        <div class="left">
            <div class="img-cover">
                <img src="<?php echo esc_url(get_field('profile_image')['url']); ?>" alt="<?php echo esc_attr(get_field('profile_image')['alt']); ?>" class="cover">
            </div>
        </div>
        <div class="right">
            <?php the_field('bio'); ?>
            <div class="spirit-animal">
                <img src="<?php the_field('spirit_animal'); ?>" alt="spirit animal">
                <h4 class="text">Spirit Animal</h4>
            </div>
        </div>
    </section>
    <section class="s5-drifter">
        <p class="gotham -header">SOME OF <?php echo getFirstName(get_the_title())  ?>'S FAVORITES </p>
        <div class="favorites">
            <?php if( have_rows('favorites') ): ?>
                <?php while( have_rows('favorites') ): the_row(); ?>
                    <div class="-block">
                        <h4><?php the_sub_field('question'); ?></h4>
                        <p class="gotham"><?php the_sub_field('answer'); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <!-- <a href="#" class="main-button nc">MORE FAVORITES THIS WAY</a> -->
    </section>

    <!-- WILL SHOW BLOG POSTS ONLY IF THERE ARE ANY BY CURRENT DRIFTER -->
    <?php   
            $employee = getFirstName(get_the_title());
            $author = new WP_Query(array(
                'post_type' => 'post',
                'post_status' => array('publish'),
                'author_name' => $employee,
                'posts_per_page' => -1
            ));
            //CHECKS AND SEES IF DRIFTER HAS ANY BLOG POSTS
            if($author->have_posts()) :
    ?>
        <section class="s6-drifter postsBy">
            <div class="left">
                <h2 class="b58">Posts by <?php echo getFirstName(get_the_title()) ?></h2>
                <div class="img-cover">
                    <!-- <img src="<?php the_field('cartoon_image') ?>" alt="" class="cover"> -->
                </div>
            </div>
            <div class="right">
                <ul>
                    <?php
                        $i = 0;
                        
                            while($author->have_posts() && $i<=5) : $author->the_post();
                            if(get_the_title() != 'Auto Draft'): 
                    ?>
                        <li>
                            <a href="<?php the_permalink(); ?>">
                                <h5 class="gotham b20"><?php the_time('M j'); ?></h5>
                                <h2 class="b40"> <?php the_title(); ?> </h2>
                            </a>
                        </li>
                            <?php else:?>
                                <li>
                                    <h2 style="color:white;" class="b40">No Posts at this time.</h2>
                                </li>
                    <?php endif; $i++; endwhile;?>
                </ul>
                <ul class="show-more">
                    <?php while($author->have_posts() && $i > 5): $author->the_post(); ?>

                        <li>
                            <a href="<?php the_permalink(); ?>">
                                <h5 class="gotham"><?php the_time('M j'); ?></h5>
                                <h2> <?php the_title(); ?> </h2>
                            </a>
                        </li>

                        <?php $i++; endwhile; ?>
                </ul>
                <?php if($i >= 5): ?>
                    <div class="main-button show-more-button -navy ">View More Posts</div>
                <?php endif;?>
            </div>
        </section>
    <?php endif;  wp_reset_postdata(); ?>
    
        <?php
            $next_post = get_next_post();
            if (!empty( $next_post )): 
        ?>
            <section class="next-drifter">
                <div class="line"></div>
                <p class="gotham">NEXT UP</p>
                <div class="next"><a class="next dot-link " href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>"><?php echo esc_attr( $next_post->post_title ); ?></a></div>
            </section>
        <?php endif; ?>
        
</main>
<script>
    $('.s4-drifter .right p').addClass('gotham');
</script>

<?php get_footer(); ?>