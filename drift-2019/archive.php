<?php get_header(); ?>


<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<?php the_title('<h1 class="posts-title">', '</h1>'); ?>
					<h2 class="the-date"><?php echo the_time('F j, Y'); ?></h2>
					
				</a>
			</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>

	



<?php get_footer(); ?>