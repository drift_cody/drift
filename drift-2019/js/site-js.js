$(".menu-activator").click(function () {
  $menuOpen = $(".nav-container");
  $menuOpen.toggleClass("active");
  $("body").removeClass("drop");
  $("header").toggleClass("menuOpen");
  if ($menuOpen.hasClass("active")) {
    $(this).attr("aria-expanded", "true");
    $(".header-section .dot-link").css("opacity", "0");
    $("body").addClass("no-scroll");
    $("html").addClass("no-scroll");
    $(".hidden-nav").attr("aria-hidden", "false");
  } else {
    $(this).attr("aria-expanded", "false");
    $(".header-section .dot-link").css("opacity", "1");
    $("body").removeClass("no-scroll");
    $("html").removeClass("no-scroll");
    $(".hidden-nav").attr("aria-hidden", "true");
  }
});
$(".close-button").click(function () {
  $(".hidden-nav").removeClass("active");
});

$(".show-more-button").click(function () {
  $(".show-more").slideToggle("slow");
  if ($(this).hasClass("open")) {
    $(this).html("SHOW MORE POSTS");
    $(this).removeClass("open");
    $(this).addClass("closed");
  } else {
    $(this).html("SHOW LESS POSTS");
    $(this).removeClass("closed");
    $(this).addClass("open");
  }
});

//ORIGIN PAGE YEAR NAVIGATION//////
$(".year-nav ul li").click(function () {
  var year = "." + $(this).attr("id");

  //removes active from all year list items
  $(".year-nav ul li").each(function (i) {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    }
  });
  //gives clicked list item an active state
  $(this).addClass("active");
  $(".year").removeClass("active");
  $(year).addClass("active");
});
//END ORIGIN PAGE YEAR NAVIGATION/////\
//ADD CLASSES TO WYSYWIG FIELDS FOR STYLING
//HOME PAGE
$(".s5-home .top h2").addClass("b58");
$(".service h2").addClass("b40");
$(".case-study .info h2").addClass("b40");
$(".s3-home .text h2").addClass("b40");
//CONTACT
$(".s2-contact .-content h1").addClass("b58");
//CULTURE
$(".s1-culture h1").addClass("b58");
$(".s2-culture h2").addClass("b40");
//LOGOS
$(".s2-logos .text h1").addClass("b58");
$(".s2-logos .text p").addClass("p-25");
$(".s4-logos .text h2").addClass("b58");
//DRIFTERS
$(".s4-drifter .right h2").addClass("b40");
//SERVICES
$(".s3-services .right h2").addClass("b65");
//CHILDREN
$(".s1-services-child .content .left h1").addClass("b100");
$(".s2-services-child .content .right p").addClass("p-25");
$(".s4-services-child h2").addClass("b58");
$(".s5-services-child .side-by-side .left p").addClass("p-25");
//WORK
$(".s3-work .text-row h2").addClass("b58");
//CAREERS
$(".s4_career .core-values__text h2").addClass("b58");

$(document).ready(function () {
  //SLICK SLIDERS:
  //HOME PAGE
  $(".blog-posts.mobile").slick({
    centerMode: true,
    slidesToShow: 1,
    prevArrow: false,
    nextArrow: false,
    centerPadding: "30px",
  });

  $(".localizing-b2b").click(function () {
    $("body").toggleClass("drop");
    $(".dropdown-newsletter").toggleClass("drop");
    var text = $(".localizing-b2b").text();
    console.log("inside click function text is: ", text);
    if (text === "NEWSLETTER SIGN UP") {
      $(".localizing-b2b").text("Close");
    } else {
      $(".localizing-b2b").text("NEWSLETTER SIGN UP");
    }
  });
});
//RESPONSIVE SKROLLER LINES
checkSize();

// run test on resize of the window
$(window).resize(checkSize);

function checkSize() {
  //HOMEPAGE
  //RED LINE
  if ($(window).width() < 1501) {
  }
  if ($(window).width() < 1201) {
    $(".s2-video-thumb .line").attr(
      "data-center-top",
      "height:calc(25% + 30px)"
    );
    $(".s2-video-thumb .line").attr("data-200-top", "height:calc(50% + 140px)");
    $(".s6-home .line").attr("data--500-bottom-top", "height:215px");
  }
  if ($(window).width() < 993) {
  }
}

/////////////////////////////////////////////////////
/////////////////////  ICON 3  //////////////////////
/////////////////////////////////////////////////////

///Initiation Variables
var icon_3 = document.getElementById("b3");
var topLine_3 = document.getElementById("top-line-1");
var middleLine_3 = document.getElementById("middle-line-1");
var bottomLine_3 = document.getElementById("bottom-line-1");
var state_3 = "menu"; // can be "menu" or "cross"
var topLineY_3;
var middleLineY_3;
var bottomLineY_3;
var topLeftX_3;
var topRightX_3;
var middleLeftX_3;
var middleRightX_3;
var bottomLeftX_3;
var bottomRightX_3;
var topLeftY_3;
var topRightY_3;
var middleLeftY_3;
var middleRightY_3;
var bottomLeftY_3;
var bottomRightY_3;

///Animation Variables
var segmentDuration_3 = 20;
var menuDisappearDurationInFrames_3 = segmentDuration_3;
var crossAppearDurationInFrames_3 = segmentDuration_3 * 1.5;
var crossDisappearDurationInFrames_3 = segmentDuration_3 * 1.5;
var menuAppearDurationInFrames_3 = segmentDuration_3;
var menuDisappearComplete_3 = false;
var crossAppearComplete_3 = false;
var crossDisappearComplete_3 = true;
var menuAppearComplete_3 = true;
var currentFrame_3 = 0;
var cPt_3 = { x: 50, y: 50 }; // center point
var tlPt_3 = { x: 30, y: 37 }; // top right point
var trPt_3 = { x: 70, y: 37 }; // top left point
var mlPt_3 = { x: 30, y: 50 }; // middle right point
var mrPt_3 = { x: 70, y: 50 }; // middle left point
var blPt_3 = { x: 30, y: 63 }; // bottom right point
var brPt_3 = { x: 70, y: 63 }; // bottom left point
var topLineOpacity_3 = 1;
var middleLineOpacity_3 = 1;
var bottomLineOpacity_3 = 1;

///Position Rotation
function positionRotation(centerPoint, orbitPoint, angleInRads) {
  var distance = Math.sqrt(
    Math.pow(orbitPoint.x - centerPoint.x, 2) +
      Math.pow(orbitPoint.y - centerPoint.y, 2)
  );
  orbitPoint.x = centerPoint.x + Math.cos(angleInRads) * distance;
  orbitPoint.y = centerPoint.y + Math.sin(angleInRads) * distance;
}

///Menu Disappear
function menuDisappearAnimation_3() {
  currentFrame_3++;
  if (currentFrame_3 <= menuDisappearDurationInFrames_3) {
    window.requestAnimationFrame(() => {
      var rotation = Math.PI * 0.5;
      //top line
      var tlAng = AJS.easeInBack(
        3.7179,
        3.7179 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var trAng = AJS.easeInBack(
        5.7068,
        5.7068 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, tlPt_3, tlAng);
      positionRotation(cPt_3, trPt_3, trAng);
      topLine_3.setAttribute(
        "d",
        "M" +
          tlPt_3.x +
          "," +
          tlPt_3.y +
          " L" +
          trPt_3.x +
          "," +
          trPt_3.y +
          " Z"
      );
      //middle line
      var mlAng = AJS.easeInBack(
        Math.PI,
        Math.PI + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var mrAng = AJS.easeInBack(
        0,
        rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, mlPt_3, mlAng);
      positionRotation(cPt_3, mrPt_3, mrAng);
      middleLine_3.setAttribute(
        "d",
        "M" +
          mlPt_3.x +
          "," +
          mlPt_3.y +
          " L" +
          mrPt_3.x +
          "," +
          mrPt_3.y +
          " Z"
      );
      //bottom line
      var blAng = AJS.easeInBack(
        2.5652,
        2.5652 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var brAng = AJS.easeInBack(
        0.5763,
        0.5763 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, blPt_3, blAng);
      positionRotation(cPt_3, brPt_3, brAng);
      bottomLine_3.setAttribute(
        "d",
        "M" +
          blPt_3.x +
          "," +
          blPt_3.y +
          " L" +
          brPt_3.x +
          "," +
          brPt_3.y +
          " Z"
      );
      //recursion
      menuDisappearAnimation_3();
    });
  } else {
    currentFrame_3 = 0;
    menuDisappearComplete_3 = true;
    openMenuAnimation_3();
  }
}

///Cross Appear
function crossAppearAnimation_3() {
  currentFrame_3++;
  if (currentFrame_3 <= crossAppearDurationInFrames_3) {
    tlPt_3 = { x: 50, y: 28.7867 };
    trPt_3 = { x: 50, y: 71.2132 };
    mlPt_3 = { x: 28.7867, y: 50 };
    mrPt_3 = { x: 71.2132, y: 50 };
    window.requestAnimationFrame(() => {
      var rotation = Math.PI * 0.75;
      //top line
      var tlAng = AJS.easeOutBack(
        Math.PI,
        Math.PI + rotation,
        crossAppearDurationInFrames_3,
        currentFrame_3
      );
      var trAng = AJS.easeOutBack(
        0,
        rotation,
        crossAppearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, tlPt_3, tlAng);
      positionRotation(cPt_3, trPt_3, trAng);
      topLine_3.setAttribute(
        "d",
        "M" +
          tlPt_3.x +
          "," +
          tlPt_3.y +
          " L" +
          trPt_3.x +
          "," +
          trPt_3.y +
          " Z"
      );
      //center line
      var mlAng = AJS.easeOutBack(
        Math.PI * 1.5,
        Math.PI * 1.5 + rotation,
        crossAppearDurationInFrames_3,
        currentFrame_3
      );
      var mrAng = AJS.easeOutBack(
        Math.PI * 0.5,
        Math.PI * 0.5 + rotation,
        crossAppearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, mlPt_3, mlAng);
      positionRotation(cPt_3, mrPt_3, mrAng);
      middleLine_3.setAttribute(
        "d",
        "M" +
          mlPt_3.x +
          "," +
          mlPt_3.y +
          " L" +
          mrPt_3.x +
          "," +
          mrPt_3.y +
          " Z"
      );
      //bottom line
      bottomLine_3.style.opacity = 0;
      //recursion
      crossAppearAnimation_3();
    });
  } else {
    currentFrame_3 = 0;
    crossAppearComplete_3 = true;
    openMenuAnimation_3();
  }
}

///Combined Open Menu Animation
function openMenuAnimation_3() {
  if (!menuDisappearComplete_3) {
    menuDisappearAnimation_3();
  } else if (!crossAppearComplete_3) {
    crossAppearAnimation_3();
  }
}

///Cross Disappear
function crossDisappearAnimation_3() {
  currentFrame_3++;
  if (currentFrame_3 <= crossDisappearDurationInFrames_3) {
    window.requestAnimationFrame(() => {
      var rotation = Math.PI * 0.75;
      //top line
      var tlAng = AJS.easeInBack(
        Math.PI * 1.75,
        Math.PI * 1.75 + rotation,
        crossDisappearDurationInFrames_3,
        currentFrame_3
      );
      var trAng = AJS.easeInBack(
        Math.PI * 0.75,
        Math.PI * 0.75 + rotation,
        crossDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, tlPt_3, tlAng);
      positionRotation(cPt_3, trPt_3, trAng);
      topLine_3.setAttribute(
        "d",
        "M" +
          tlPt_3.x +
          "," +
          tlPt_3.y +
          " L" +
          trPt_3.x +
          "," +
          trPt_3.y +
          " Z"
      );
      //center line
      var mlAng = AJS.easeInBack(
        Math.PI * 2.25,
        Math.PI * 2.25 + rotation,
        crossDisappearDurationInFrames_3,
        currentFrame_3
      );
      var mrAng = AJS.easeInBack(
        Math.PI * 1.25,
        Math.PI * 1.25 + rotation,
        crossDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, mlPt_3, mlAng);
      positionRotation(cPt_3, mrPt_3, mrAng);
      middleLine_3.setAttribute(
        "d",
        "M" +
          mlPt_3.x +
          "," +
          mlPt_3.y +
          " L" +
          mrPt_3.x +
          "," +
          mrPt_3.y +
          " Z"
      );
      //bottom line
      bottomLine_3.style.opacity = 0;
      //recursion
      crossDisappearAnimation_3();
    });
  } else {
    middleLine_3.style.opacity = "1";
    currentFrame_3 = 0;
    crossDisappearComplete_3 = true;
    closeMenuAnimation_3();
  }
}

///Menu Appear
function menuAppearAnimation_3() {
  currentFrame_3++;
  if (currentFrame_3 <= menuAppearDurationInFrames_3) {
    tlPt_3 = { x: 37, y: 70 };
    trPt_3 = { x: 37, y: 30 };
    mlPt_3 = { x: 50, y: 70 };
    mrPt_3 = { x: 50, y: 30 };
    bottomLine_3.style.opacity = 1;
    window.requestAnimationFrame(() => {
      var rotation = Math.PI * 0.5;
      //top line
      var tlAng = AJS.easeOutBack(
        2.1471,
        2.1471 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var trAng = AJS.easeOutBack(
        4.136,
        4.136 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, tlPt_3, tlAng);
      positionRotation(cPt_3, trPt_3, trAng);
      topLine_3.setAttribute(
        "d",
        "M" +
          tlPt_3.x +
          "," +
          tlPt_3.y +
          " L" +
          trPt_3.x +
          "," +
          trPt_3.y +
          " Z"
      );
      //middle line
      var mlAng = AJS.easeOutBack(
        Math.PI * 0.5,
        Math.PI * 0.5 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var mrAng = AJS.easeOutBack(
        Math.PI * 1.5,
        Math.PI * 1.5 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, mlPt_3, mlAng);
      positionRotation(cPt_3, mrPt_3, mrAng);
      middleLine_3.setAttribute(
        "d",
        "M" +
          mlPt_3.x +
          "," +
          mlPt_3.y +
          " L" +
          mrPt_3.x +
          "," +
          mrPt_3.y +
          " Z"
      );
      //bottom line
      var blAng = AJS.easeOutBack(
        0.9944,
        0.9944 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      var brAng = AJS.easeOutBack(
        5.2887,
        5.2887 + rotation,
        menuDisappearDurationInFrames_3,
        currentFrame_3
      );
      positionRotation(cPt_3, blPt_3, blAng);
      positionRotation(cPt_3, brPt_3, brAng);
      bottomLine_3.setAttribute(
        "d",
        "M" +
          blPt_3.x +
          "," +
          blPt_3.y +
          " L" +
          brPt_3.x +
          "," +
          brPt_3.y +
          " Z"
      );
      //recursion
      menuAppearAnimation_3();
    });
  } else {
    currentFrame_3 = 0;
    menuAppearComplete_3 = true;
  }
}

///Close Menu Animation
function closeMenuAnimation_3() {
  if (!crossDisappearComplete_3) {
    crossDisappearAnimation_3();
  } else if (!menuAppearComplete_3) {
    menuAppearAnimation_3();
  }
}

///Events
icon_3.addEventListener("click", () => {
  console.log("hello");
  if (state_3 === "menu") {
    openMenuAnimation_3();
    state_3 = "cross";
    crossDisappearComplete_3 = false;
    menuAppearComplete_3 = false;
  } else if (state_3 === "cross") {
    closeMenuAnimation_3();
    state_3 = "menu";
    menuDisappearComplete_3 = false;
    crossAppearComplete_3 = false;
  }
});

//MENU SCROLL COLOR CHANGE FOR A LATER DATE
//OLD CODE
// var changeSectionTop = $("section.s5-culture").offset().top - $(window).scrollTop();
// var menuTop = $(".side-nav-container").offset().top - $(window).scrollTop();
// var menuHeight = $(".side-nav-container").height()+100;
// var changeSectionHeight = $("section.s5-culture").height();
// var changeSectionBottom = changeSectionTop + changeSectionHeight;
// // console.log();
// // console.log("Menu bottom is: "+menuHeight);
// // // console.log("Menu Height is: "+(menuHeight-100));
// // console.log("section height is: "+changeSectionHeight);
// // console.log("Change Section Top is: "+changeSectionTop);
// // console.log("Section Bottom is: "+changeSectionBottom);
// // console.log("Menu Height is: "+menuHeight);
// // console.log("----------------------------------------");
// if(changeSectionTop <= (menuHeight) && changeSectionTop > 100){
//   //change width of top menu in relation to change section
//   var perChng2 = (((changeSectionTop-menuHeight)/(menuHeight-130))*-1)*100;
//   var menuWidth = Math.round(100 - perChng2);
//   //console.log("in first part");
//   //console.log("perChng2: "+menuWidth);
//   $(".side-nav.top").css('width', menuWidth+'%');
// }else if(changeSectionTop < 100 && changeSectionBottom > menuHeight){
//   //console.log("inside second part")
//   $(".alpha-container").css("width", "0");
//   $(".side-nav.bottom").css("z-index", "2");
// }else if((changeSectionBottom) <= (menuHeight)){ //while it is fully inside of the change section
//   console.log("in third part");
//   var heightChange = (changeSectionBottom)/menuHeight;
//   //console.log(heightChange);
//   var heightChange = ((changeSectionBottom)/menuHeight)*100;
//   //console.log(heightChange);
//   heightChange = (100 - heightChange);
//   //console.log(heightChange);
//   if(heightChange<100){
//     $(".alpha-container").css("width",heightChange+"%");
//   }else{
//     $(".alpha-container").css("width",heightChange+"%");
//   }
// }
///END OLD CODE
// //gets all sections on page and their postions
// function getAllSections(){
//   var sections=[];
//   $('body').find('section').each(function(){
//     var sectionTop =$(this).offset().top;
//     var sectionTopOffset = $(this).offset().top - $(window).scrollTop();
//     var sectionHeight = $(this).outerHeight(true);
//     var sectionBottom = sectionTop + sectionHeight;
//     var sectionClass = $(this).attr("identifier");
//     var sectionColor = $(this).css('background');
//     var section = {"section":sectionClass, "top":sectionTop, "top-offset":sectionTopOffset, "bottom":sectionBottom, "height":sectionHeight, "background-color":sectionColor};
//     sections.push(section);
//     //console.log(section);
//   });
//   //console.log('------------------------------------');
//   return sections;
// }
// //checks to see where the menu is
// function getCurrentSection(top, sectionArray){
//   var menu = $('.side-nav-container');
//   var menuTop = menu.offset().top;
//   var currentSection;
//   if(menu.css('top')===(top+"px")){
//    $.each(sectionArray,function(index,val){
//       if(menuTop >= val.top ){
//         currentSection = val.section;
//       }
//    });
//    return currentSection;
//   }
// }

// $( document ).ready(function() {
//   //ADD CLASSES TO WYSYWIG FIELDS FOR STYLING
//     //HOME PAGE
//       $(".s5-home .top h2").addClass("b58");
//       $(".service h2").addClass("b40");
//       $(".case-study .info h2").addClass("b40");
//       $(".s3-home .text h2").addClass("b40");

//   //SLICK SLIDERS:
//     //HOME PAGE
//       $('.blog-posts.mobile').slick({
//         centerMode: true,
//         slidesToShow: 1,
//         prevArrow: false,
//         nextArrow: false,
//       });

//   //STICKY MENUS-color change
//       //CULTURE
//       //GET ALL SECTIONS ON PAGE

//       $(document).on('scroll', function(){
//         var initialMenuTopValue=100;
//         var sections = getAllSections();
//         var currentSection = getCurrentSection(initialMenuTopValue, sections);
//         var lastSection = sections[sections.length-1][section];
//         console.log("last-section: "+lastSection);

//         // $.each(sections, function(index){
//         //   console.log(sections[index]);
//         // })
//         //menuColorInit(100);
//         // var height = $('.s2-culture').outerHeight(true);
//         // var top = $('.s2-culture').offset().top-$(window).scrollTop();
//         // var bottom = top + height;
//         // console.log("s2 height: " +height);
//         // console.log("s2 top: "+top);
//         // console.log("s2 bottom: "+bottom);
//       });

//});
