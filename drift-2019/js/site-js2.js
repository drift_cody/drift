function swapForm() {
	console.log('inside swapForm');
	if ($('.wpcf7-response-output').hasClass('wpcf7-mail-sent-ok')) {
		console.log('inside if statement');
		$('.multi-step-contact').css('opacity', '0');
		$('.multi-step-contact').slideUp();
		setTimeout($('.message-in-route').slideDown(), 3000);
	}
}
$(document).ready(function() {
	$('.s1-work-overview .text h1').addClass('b58');

	// $("input[value='Branding']").parent().addClass("active");

	$("input[name='radio-525[]']").change(function() {
		$(this).parent().toggleClass('active');
	});

	$('.start-it').click(function(e) {
		e.preventDefault();
		$('.contact-start').slideUp();
		$('.multi-step-contact').slideDown();
	});

	$('.fluid-video').fitVids();
});

$(".multi-step-contact input[type='submit']").click(function() {
	setTimeout(swapForm, 2000);
});
