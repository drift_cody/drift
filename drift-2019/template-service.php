<?php if( have_rows('flexible_content') ): ?>
    <?php while( have_rows('flexible_content') ): the_row(); ?>
            
        <?php if( get_row_layout() == 'image_row' ): ?>
            <div class="fc-block image-row">
                <?php if( have_rows('images') ): ?>
                    <?php while( have_rows('images') ): the_row(); ?>
                        <div class="image">
                            <img class="work-img" src="<?php echo esc_url(get_sub_field('image')['url']); ?>" />
                        </div>
                        <?php if(get_sub_field('text')): ?>
                            <div class="text">
                                <div class="content">
                                    <?php the_sub_field('text'); ?>
                                </div>
                            </div>
                        <?php endif; ?>						
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        
        
        <?php if( get_row_layout() == 'bold_text' ): ?>
            <div class="fc-block bold_text">
                <div class="circle icon-checkmark"></div>
                <h2 class="b58"><?php the_sub_field('bold_text');?></h2>
            </div>
        <?php endif; ?>

        <?php if( get_row_layout() == 'side_by_side_text' ): ?>
            <div class="fc-block side-by-side">
                <div class="left">
                    <?php the_sub_field('left_text'); ?>
                </div>
                <div class="right">
                    <?php the_sub_field('right_text');?>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>