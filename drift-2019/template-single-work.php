<?php if (have_rows('flexible_content')) : ?>
	<?php while (have_rows('flexible_content')) : the_row(); ?>

		<!-- Begin image row -->
		<?php if (get_row_layout() == 'image_row') : ?>
			<div class="image-row-block">
				<?php if (have_rows('images')) : ?>
					<?php while (have_rows('images')) : the_row(); ?>
						<div class="image">
							<img class="work-img" src="<?php the_sub_field('image'); ?>" />
						</div>
						<?php if (get_sub_field('text')) : ?>
							<div class="text">
								<div class="content">
									<?php the_sub_field('text'); ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- End image row -->

		<!-- Begin text row -->
		<?php if (get_row_layout() == 'text_row') : ?>
			<div class=" block text-row">
				<div class="text contained lapture b58">
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End text row -->

		<!-- Begin static web page screenshots -->
		<?php if (get_row_layout() == 'static_web_page_screenshots') : ?>
			<div class=" block static-web-screenshots-row block">
				<div class="left-column">
					<div class="text">
						<?php the_sub_field('text'); ?>
					</div>
					<?php if (get_sub_field('left_image')) : ?>
						<img src="<?php the_sub_field('left_image'); ?>" />
					<?php endif; ?>
				</div>
				<div class="right-column">
					<?php if (get_sub_field('right_image')) : ?>
						<img src="<?php the_sub_field('right_image'); ?>" />
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End static web page screenshots -->

		<!-- Begin scrolling text -->
		<?php if (get_row_layout() == 'scrolling_text') : ?>
			<div class="scrolling-text" style="background:<?php the_sub_field('background_color'); ?>">
				<div class="sticky-anchor"></div>
				<div class="text sticky">
					<?php the_sub_field('text'); ?>
				</div>
				<div class="image">
					<img src="<?php the_sub_field('image'); ?>" />
				</div>
			</div>
			<div class="scrolling-text-end"></div>
		<?php endif; ?>
		<!-- End scrolling text -->

		<!-- Begin video row -->
		<?php if (get_row_layout() == 'video_row') : ?>
			<div class="video-row ">
				<div class="image">
					<img src="<?php the_sub_field('image'); ?>" />
				</div>
				<div class="video">
					<?php if (get_sub_field('video_type') == 'Upload') : ?>
						<div class="video-container">
							<video autoplay muted loop>
								<source src="<?php the_sub_field('video_upload'); ?>">
							</video>
						</div>
					<?php endif; ?>
					<?php if (get_sub_field('video_type') == 'YouTube') : ?>
						<div class="video-container youtube">
							<img src="<?php the_sub_field('thumbnail'); ?>" />
							<div class="play"></div>
							<div class="video-popup">
								<div class="vid-width">
									<div class="vid-con">
										<?php the_sub_field('youtube_embed'); ?>
										<div class="close-it"></div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<!-- End video row -->

		<!-- Begin Full Video Row -->
		<?php if (get_row_layout() == 'full_video_row') : ?>
			<div class="full-video-row">
				<video width="100%" autoplay loop class="full-video-row__video" muted="muted">
					<source src="<?php echo esc_url(get_sub_field('video_file'));?>" type="video/mp4">
				</video>
			</div>
		<?php endif;?>
		<!-- End Full Video Row -->

		<!-- Begin stats row -->
		<?php if (get_row_layout() == 'stats_row') : ?>
			<div class=" block stats-row">
				<div>
					<?php if (get_sub_field('header_text')) : ?>
						<p class="all-in"><?php the_sub_field('header_text'); ?></p>
					<?php else : ?>
						<p class="all-in">Its all in the stats</p>
					<?php endif; ?>
					<div class="stats">
						<?php if (have_rows('stats')) : ?>
							<?php while (have_rows('stats')) : the_row(); ?>
								<div class="stat">
									<h1 class="big-number"><?php the_sub_field('big_text'); ?></h1>
									<h3><?php the_sub_field('small_text'); ?></h3>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<!-- End stats row -->
		<!-- Begin Slideshow -->
		<?php if (get_row_layout() == 'slideshow') : ?>
			<div class=" slickSlideShow">
				<?php if (have_rows('images')) : ?>
					<?php while (have_rows('images')) : the_row(); ?>
						<div class="img-cover">
							<img src="<?php the_sub_field('image'); ?>" alt="">
							<div class="buttons">
								<div class="slide-left"></div>
								<div class="slide-right"></div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- End Slideshow -->

		<!-- Begin Fun Facts -->
		<?php if (get_row_layout() == 'fun_facts') : ?>
			<div class=" block fun-facts">
				<h6 class="gotham">FUN FACTS OF THE YEAR</h6>
				<ul class="ns">
					<?php if (have_rows('facts')) : ?>
						<?php while (have_rows('facts')) : the_row(); ?>
							<li>
								<p class="lapture"><?php the_sub_field('question'); ?></p>
								<p class="gotham"><?php the_sub_field('answer'); ?></p>
							</li>
						<?php endwhile; ?>
					<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- End Fun Facts -->

		<!-- IMAGE / VIDEO ROW 1X3 -->
		<?php if (get_row_layout() == 'image_video_row_1x3') : ?>

			<div class="image-video-3x">
				<?php if (have_rows('content')) : ?>
					<?php while (have_rows('content')) : the_row(); ?>

						<div class="image-video-3x__cell">
							<?php if (get_sub_field('radio_button') === 'Video') : ?>
								<div class="image-video-3x__content-container">
									<div class="image-video-3x__play-button">
										<p>PLAY</p>
									</div>
									<video class="image-video-3x__video" width="520" height="520">
										<source src="<?php echo esc_url(get_sub_field('video')); ?>" type="video/mp4">
									</video>
								</div>


							<?php else : ?>
								<div class="image-video-3x__image-container">
									<img src="<?php echo esc_url(get_sub_field('image')['url']); ?>" alt="" class="image-video-3x__image">
								</div>
							<?php endif; ?>
						</div>

				<?php endwhile;
							endif; ?>
			</div>

		<?php endif; ?>
		<!-- END IMAGE / VIDEO ROW 1X3 -->
	<?php endwhile; ?>
<?php endif; ?>