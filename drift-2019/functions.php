<?php
// basic stuff //////////////////////////////////////////////
register_nav_menu('primary', 'Primary Menu');
add_theme_support('post-thumbnails');
// /basic stuff /////////////////////////////////////////////



// styles/scripts ///////////////////////////////////////////
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
}

function add_theme_scripts()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css', false, '1.0.0', 'all');
    wp_enqueue_style('typekit', 'https://use.typekit.net/hpb6ivh.css', false, '1.0.0', 'all');
    wp_enqueue_style('googlefont', 'https://fonts.googleapis.com/css?family=Open+Sans&display=swap', false, '1.0.0', 'all');
    wp_enqueue_style('gothamFont', 'https://cloud.typography.com/6968136/6650012/css/fonts.css', false, '1.0.0', 'all');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', false, '1.0.0', 'all');

    wp_enqueue_style('slick', get_template_directory_uri() . '/css/slick.css', false, '1.0.0', 'all');
    wp_enqueue_style('icons', get_template_directory_uri() . '/css/Drift-2019-icon-pack.css', false, '1.0.0', 'all');

    wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('hoverintent', get_stylesheet_directory_uri() . '/js/hoverintent.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('site-js', get_stylesheet_directory_uri() . '/js/site-js.js', array('jquery'), 1.1, true);
    wp_enqueue_script('skrollr', get_stylesheet_directory_uri() . '/js/skrollr.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('imagesloaded', get_stylesheet_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('three', get_stylesheet_directory_uri() . '/js/three.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('tween', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js', array('jquery'), 1.1, true);
    wp_enqueue_script('hover', get_stylesheet_directory_uri() . '/js/hover.js', array('jquery'), 1.1, true);

    wp_enqueue_script('site-js2', get_stylesheet_directory_uri() . '/js/site-js2.js', array('jquery'), 1.1, true);
    wp_enqueue_script('fitvid', get_stylesheet_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), 1.1, true);


    // Register the script
    wp_register_script( 'awscript', get_stylesheet_directory_uri() . '/js/mailchimp.js', array('jquery') );
 
    // Localize the script with new data
    $script_array = array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'security' => wp_create_nonce("subscribe_user"),
    );
    wp_localize_script( 'awscript', 'aw', $script_array );
 
    // Enqueued script with localized data.
    wp_enqueue_script( 'awscript' );

}
add_action('wp_enqueue_scripts', 'add_theme_scripts');
// /styles/scripts //////////////////////////////////////////



// options page /////////////////////////////////////////////
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}
// /options page ////////////////////////////////////////////



// custom post types ////////////////////////////////////////
function work_post_type()
{

    $labels = array(
        'name'                  => 'Work',
        'singular_name'         => 'Work',
        'menu_name'             => 'Work',
        'name_admin_bar'        => 'Work',
        'archives'              => 'Item Archives',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Work',
        'labels'                => $labels,
        'supports'              => array(),
        'taxonomies'            => array('category', 'post_tag'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type('work', $args);
}
add_action('init', 'work_post_type', 0);

// Register Custom Post Type
function single_drifter()
{

    $labels = array(
        'name'                  => 'Drifters',
        'singular_name'         => 'Drifter',
        'menu_name'             => 'Drifters',
        'name_admin_bar'        => 'Drifter',
        'archives'              => 'Employee Archives',
        'attributes'            => 'Drifter Attributes',
        'parent_item_colon'     => '',
        'all_items'             => 'All Drifters',
        'add_new_item'          => 'Add New Drifter',
        'add_new'               => 'Add Drifter',
        'new_item'              => 'New Drifter',
        'edit_item'             => 'Edit Drifter',
        'update_item'           => 'Update Drifter',
        'view_item'             => 'View Drifter',
        'view_items'            => 'View Drifters',
        'search_items'          => 'Search Drifters',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Drifter',
        'description'           => 'Drift Employees',
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'custom-fields'),
        'hierarchical'          => false,
        'menu_icon'             => 'dashicons-drift',
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type('drifter', $args);
}
add_action('init', 'single_drifter', 0);

// /custom post types ///////////////////////////////////////



// waldo ////////////////////////////////////////////////////
include('waldo-master/waldo.php');

function wpse87681_enqueue_custom_stylesheets()
{
    if (!is_admin()) {
        wp_enqueue_style('mytheme-custom', get_template_directory_uri() . '/waldo.css');
    }
}
add_action('wp_enqueue_scripts', 'wpse87681_enqueue_custom_stylesheets', 11);

add_action('after_setup_theme', 'wpdocs_theme_setup');
function wpdocs_theme_setup()
{
    add_image_size('xlarge', 1400);
    add_image_size('xxlarge', 1800);
    add_image_size('xxxlarge', 2200);
    add_image_size('xxxxlarge', 2500);
}
// /waldo ///////////////////////////////////////////////////



// acf image helpers ////////////////////////////////////////////////////
function ar_responsive_image($image_id, $image_size, $max_width)
{
    // check the image ID is not blank
    if ($image_id != '') {
        // set the default src image size
        $image_src = wp_get_attachment_image_url($image_id, $image_size);
        // set the srcset with various image sizes
        $image_srcset = wp_get_attachment_image_srcset($image_id, $image_size);
        // generate the markup for the responsive image
        echo 'src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';
    }
}

function acf_srcset($image_id, $image_size, $max_width)
{
    // check the image ID is not blank
    if ($image_id != '') {
        // set the default src image size
        $image_src = wp_get_attachment_image_url($image_id, 'xxlarge');
        // set the srcset with various image sizes
        $image_srcset = wp_get_attachment_image_srcset($image_id, $image_size);
        // generate the markup for the responsive image
        echo 'src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';
    }
}
// /acf image helpers ///////////////////////////////////////////////////

/////////////////AMOUNT OF TAGS SHOWN ON POST///////////////////////////

add_filter('term_links-post_tag', 'limit_to_five_tags');
function limit_to_five_tags($terms)
{
    return array_slice($terms, 0, 5, true);
}


//////////////END AMOUNT OF TAGS SHOWN ON POST///////////////////////////










/////CUSTOM FUNCTIONS////////////////////////////////////////////////////////////
function getFirstName($str)
{
    $words = explode(" ", $str);
    return $words[0];
}

//USED ON THE DRIFTER SINGLE PAGE
function chooseIcon($str)
{
    $str = strtolower($str);
    $words = explode(" ", $str);
    switch ($words[0]) {
        case 'cody':
            echo 'icon-lightning-bolt';
            break;
        case 'gavin':
            echo 'icon-lightning-bolt';
            break;
        default:
            echo 'icon-lightning-bolt';
    }
}


add_action('wp_ajax_subscribe_user', 'subscribe_user_to_mailchimp');
add_action('wp_ajax_nopriv_subscribe_user', 'subscribe_user_to_mailchimp');
 
function subscribe_user_to_mailchimp() {
    check_ajax_referer('subscribe_user', 'security');
    $email = $_POST['email'];
    $audience_id = 'b1e55c1c2d';
    $api_key = '46002b9cffa4f4fbe73fed4fefd219e8-us12';
    $data_center = substr($api_key,strpos($api_key,'-')+1);
    $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists/'. $audience_id .'/members';
    $auth = base64_encode( 'user:' . $api_key );
    $arr_data = json_encode(array( 
        'email_address' => $email, 
        'status' => 'subscribed' //pass 'subscribed' or 'pending'
    ));
 
    $response = wp_remote_post( $url, array(
            'method' => 'POST',
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => "Basic $auth"
            ),
            'body' => $arr_data,
        )
    );
 
    if ( is_wp_error( $response ) ) {
       $error_message = $response->get_error_message();
       echo "Something went wrong: $error_message";
    } else {
        $status_code = wp_remote_retrieve_response_code( $response );
        switch ($status_code) {
            case '200':
                echo 'testing';
                break;
            case '400':
                $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
                echo $api_response['title'];
                break;
            default:
                echo 'Something went wrong. Please try again.' . $status_code;
                break;
        }
    }
    wp_die();
}