<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<p>test</p>
			<?php
			if ( have_posts() ) {
			    while ( have_posts() ) {
			        the_post();
			        the_content();
			    }
			}
			wp_footer();
			?>

		</div>
	</div>
</div>

<?php get_footer(); ?>